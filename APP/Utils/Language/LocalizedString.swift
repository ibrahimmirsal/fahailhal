
import Foundation
struct  AlertLocalizedString
{
    static let countrycityresponsce = "response"
    static let tripResponse = "tripresponse"
    static let driverResponse = "driverresponse"
    static let Saved_Other_Data = "savedOtherdata"
    private static let alertKey = "alert"
    static let AppleTestMobile = "201095379080"
    static let HOME_RESPONSE = "home_response"
    static let SPLASH_RESPONSE = "spalsh_response"
    static var Continue: String {
        return Localization.localizedString(alertKey, stringValue: "Continue")
    }
    static var ok: String { return Localization.localizedString(alertKey, stringValue: "ok") }
    static var yes: String { return Localization.localizedString(alertKey, stringValue: "yes") }
    static var no: String { return Localization.localizedString(alertKey, stringValue: "no") }
    static var cancel: String { return Localization.localizedString(alertKey, stringValue: "cancel") }
    static var next: String { return Localization.localizedString(alertKey, stringValue: "next") }
    static var previous: String { return Localization.localizedString(alertKey, stringValue: "previos") }
    static var done: String { return Localization.localizedString(alertKey, stringValue: "done") }
    static var error: String { return Localization.localizedString(alertKey, stringValue: "error") }
    static var alert: String { return Localization.localizedString(alertKey, stringValue: "alert")}
    static var whatsapp: String { return Localization.localizedString(alertKey, stringValue: "whatsapp")}
    static var facebook: String { return Localization.localizedString(alertKey, stringValue: "facebook")}
    static var twitter: String { return Localization.localizedString(alertKey, stringValue: "twitter")}
    static var instagram: String { return Localization.localizedString(alertKey, stringValue: "instagram") }
    static var Notes: String { return Localization.localizedString(alertKey, stringValue: "Notes") }
    static var pound: String { return Localization.localizedString(alertKey, stringValue: "pound") }
    static var monthly: String { return Localization.localizedString(alertKey, stringValue: "monthly") }
    static var error_connection: String { return Localization.localizedString(alertKey, stringValue: "error_connection") }
    static var expected_error: String { return Localization.localizedString(alertKey, stringValue: "expected_error") }
    static var retry: String { return Localization.localizedString(alertKey, stringValue: "retry") }
    static var choose: String { return Localization.localizedString(alertKey, stringValue: "choose") }
    static var type: String { return Localization.localizedString(alertKey, stringValue: "type") }
    static var call_message: String { return Localization.localizedString(alertKey, stringValue: "call_message") }
}

struct  LanguageLocalizedString
{
    private static let key = "language"
    
    static var language: String { return Localization.localizedString(key, stringValue: "language") }
    static var arabic: String { return Localization.localizedString(key, stringValue: "arabic") }
    static var english: String { return Localization.localizedString(key, stringValue: "english") }
    static var restart: String { return Localization.localizedString(key, stringValue: "restart") }
    static var restartMessage: String { return Localization.localizedString(key, stringValue: "restartMessage") }
    static var update: String { return Localization.localizedString(key, stringValue: "update") }
    static var updateTitle: String { return Localization.localizedString(key, stringValue: "updateTitle") }
    static var Changelanguage: String { return Localization.localizedString(key, stringValue: "Changelanguage") }
    static var Driver: String { return Localization.localizedString(key, stringValue: "Driver") }
    static var Passenger: String { return Localization.localizedString(key, stringValue: "Passenger") }
    static var camera: String { return Localization.localizedString(key, stringValue: "camera") }
    static var Library: String { return Localization.localizedString(key, stringValue: "Library") }
    static var logoutmessage: String { return Localization.localizedString(key, stringValue: "logoutmessage") }
    static var cycles: String { return Localization.localizedString(key, stringValue: "cycles") }
    static var cyclee: String { return Localization.localizedString(key, stringValue: "cyclee") }
    static var balance: String { return Localization.localizedString(key, stringValue: "balance") }
}

struct  ImagePickerLocalizedString
{
    private static let key = "imagePicker"
    static var camera: String { return Localization.localizedString(key, stringValue: "camera") }
    static var photos: String { return Localization.localizedString(key, stringValue: "photos") }
}

struct  SideMenuLocalizedString
{
    private static let key = "SideMenu"
    static var profile: String { return Localization.localizedString(key, stringValue: "profile") }
    static var myStory: String { return Localization.localizedString(key, stringValue: "myStory") }
    static var myMessage: String { return Localization.localizedString(key, stringValue: "myMessage") }
    static var about_us: String { return Localization.localizedString(key, stringValue: "about_us") }
    static var logout: String { return Localization.localizedString(key, stringValue: "logout") }
    static var tc: String { return Localization.localizedString(key, stringValue: "tc") }
    static var privacy: String { return Localization.localizedString(key, stringValue: "privacy") }
    static var language: String { return Localization.localizedString(key, stringValue: "language") }
    static var arabic: String { return Localization.localizedString(key, stringValue: "arabic") }
    static var english: String { return Localization.localizedString(key, stringValue: "english") }
}

struct  LoginLocalizedString
{
    private static let key = "Login"
    static var year: String { return Localization.localizedString(key, stringValue: "year") }
    static var code_missing: String { return Localization.localizedString(key, stringValue: "code_missing") }
    static var have_acount: String { return Localization.localizedString(key, stringValue: "have_acount") }
    static var RegisterTitle: String { return Localization.localizedString(key, stringValue: "RegisterTitle") }
    static var ForgetPassword: String { return Localization.localizedString(key, stringValue: "ForgetPassword") }
    static var ForgetPasswordBTN: String { return Localization.localizedString(key, stringValue: "ForgetPasswordBTN") }
    static var nothaveRefCode: String { return Localization.localizedString(key, stringValue: "nothaveRefCode") }
    static var haveRefCode: String { return Localization.localizedString(key, stringValue: "haveRefCode") }
    static var or: String { return Localization.localizedString(key, stringValue: "or") }
    static var login: String { return Localization.localizedString(key, stringValue: "login") }
    static var profileImage : String { return Localization.localizedString(key, stringValue: "profileImage") }
    static var LoginTitle: String { return Localization.localizedString(key, stringValue: "LoginTitle") }
    static var emailValidateMessage: String { return Localization.localizedString(key, stringValue: "InvalidEmail") }
    static var fields_required: String { return Localization.localizedString(key, stringValue: "fields_required") }
    static var password_too_short: String { return Localization.localizedString(key, stringValue: "Password too short") }
    static var email_required: String { return Localization.localizedString(key, stringValue: "email_required") }
    static var password_required: String { return Localization.localizedString(key, stringValue: "password_required") }
    static var email: String { return Localization.localizedString(key, stringValue: "email") }
    static var name: String { return Localization.localizedString(key, stringValue: "name") }
    static var password: String { return Localization.localizedString(key, stringValue: "password") }
    static var phone: String { return Localization.localizedString(key, stringValue: "phone") }
    static var signup: String { return Localization.localizedString(key, stringValue: "signup") }
    static var signupbtn: String { return Localization.localizedString(key, stringValue: "signupbtn") }
    static var address: String { return Localization.localizedString(key, stringValue: "address") }
    static var invalid_phone: String { return Localization.localizedString(key, stringValue: "invalid_phone") }
    static var phone_required: String { return Localization.localizedString(key, stringValue: "phone_required") }
    static var address_required: String { return Localization.localizedString(key, stringValue: "address_required") }
    static var welcome: String { return Localization.localizedString(key, stringValue: "welcome") }
    static var welcomedes: String { return Localization.localizedString(key, stringValue: "welcomedes")}
    static var newPassword: String { return Localization.localizedString(key, stringValue: "newPassword")}
    static var update: String { return Localization.localizedString(key, stringValue: "update")}
    static var profile: String { return Localization.localizedString(key, stringValue: "profile")}
    static var name_required: String { return Localization.localizedString(key, stringValue: "name_required")}
    static var logout_sure: String { return Localization.localizedString(key, stringValue: "logout_sure")}
    static var termscondition: String { return Localization.localizedString(key, stringValue: "termscondition")}
    static var message: String { return Localization.localizedString(key, stringValue: "message")}
    static var send: String { return Localization.localizedString(key, stringValue: "send")}
    static var signupDetail: String { return Localization.localizedString(key, stringValue: "signupDetail")}
    static var haveAcoount: String { return Localization.localizedString(key, stringValue: "haveAcoount")}
    static var agreeto: String { return Localization.localizedString(key, stringValue: "agreeto")}
    static var noAccount: String { return Localization.localizedString(key, stringValue: "noAccount")}
    static var skip: String { return Localization.localizedString(key, stringValue: "skip")}
    static var register: String { return Localization.localizedString(key, stringValue: "register")}
    static var confirmPassword: String { return Localization.localizedString(key, stringValue: "confirmPassword")}
    static var verify: String { return Localization.localizedString(key, stringValue: "verify")}
    static var verify_title: String { return Localization.localizedString(key, stringValue: "verify_title")}
    static var change_password: String { return Localization.localizedString(key, stringValue: "change_password")}
    static var active_code: String { return Localization.localizedString(key, stringValue: "active_code")}
    static var codeRequired: String { return Localization.localizedString(key, stringValue: "codeRequired")}
    static var invalidPassword: String { return Localization.localizedString(key, stringValue: "invalidPassword")}
    static var checkMail: String { return Localization.localizedString(key, stringValue: "checkMail")}
    static var camera: String { return Localization.localizedString(key, stringValue: "camera")}
    static var Gallery: String { return Localization.localizedString(key, stringValue: "Gallery")}
    static var passwordNotMatch: String { return Localization.localizedString(key, stringValue: "passwordNotMatch")}
    static var alartloginincorrect: String { return Localization.localizedString(key, stringValue: "alartloginincorrect")}
    static var notlogin: String { return Localization.localizedString(key, stringValue: "notlogin")}
    static var step1: String { return Localization.localizedString(key, stringValue: "step1")}
    static var step3: String { return Localization.localizedString(key, stringValue: "step3")}
    static var registerDetail: String { return Localization.localizedString(key, stringValue: "registerDetail")}
    static var country: String { return Localization.localizedString(key, stringValue: "country")}
    static var nationalID: String { return Localization.localizedString(key, stringValue: "nationalID")}
    static var fullName: String { return Localization.localizedString(key, stringValue: "fullName")}
    static var nationalidRequired: String { return Localization.localizedString(key, stringValue: "nationalidRequired")}
    static var chooseCountry: String { return Localization.localizedString(key, stringValue: "chooseCountry")}
    static var sendCode: String { return Localization.localizedString(key, stringValue: "sendCode")}
    static var smsMessage: String { return Localization.localizedString(key, stringValue: "smsMessage")}
    static var confirmation: String { return Localization.localizedString(key, stringValue: "confirmation")}
    static var smsVerify: String { return Localization.localizedString(key, stringValue: "smsVerify")}
    static var resendCode: String { return Localization.localizedString(key, stringValue: "resendCode")}
    static var agentGetails: String { return Localization.localizedString(key, stringValue: "agentGetails")}
    static var step4: String { return Localization.localizedString(key, stringValue: "step4")}
    static var government: String { return Localization.localizedString(key, stringValue: "government")}
    static var Area: String { return Localization.localizedString(key, stringValue: "Area")}
    static var agentName: String { return Localization.localizedString(key, stringValue: "agentName")}
    static var termConditionsRegister: String { return Localization.localizedString(key, stringValue: "termConditionsRegister")}
    static var chooseGovernment: String { return Localization.localizedString(key, stringValue: "chooseGovernment")}
    static var chooseArea: String { return Localization.localizedString(key, stringValue: "chooseArea")}
    static var step2: String { return Localization.localizedString(key, stringValue: "step2")}
    static var captainDeatails: String { return Localization.localizedString(key, stringValue: "captainDeatails")}
    static var profilePhoto: String { return Localization.localizedString(key, stringValue: "chooseArea")}
    static var licenseFront: String { return Localization.localizedString(key, stringValue: "licenseFront")}
    static var licenseBack: String { return Localization.localizedString(key, stringValue: "licenseBack")}
    static var criminalphoto: String { return Localization.localizedString(key, stringValue: "criminalphoto")}
    static var drugsPhoto: String { return Localization.localizedString(key, stringValue: "drugsPhoto")}
    static var checkUpPhoto: String { return Localization.localizedString(key, stringValue: "checkUpPhoto")}
    static var chooseAgent: String { return Localization.localizedString(key, stringValue: "chooseAgent")}
    static var mustAgreeToTermsAndCondditions: String { return Localization.localizedString(key, stringValue: "mustAgreeToTermsAndCondditions")}
    static var vehicleDetails: String { return Localization.localizedString(key, stringValue: "vehicleDetails")}
    static var vehicleType: String { return Localization.localizedString(key, stringValue: "vehicleType")}
    static var vehicleCategory: String { return Localization.localizedString(key, stringValue: "vehicleCategory")}
    static var licenseId: String { return Localization.localizedString(key, stringValue: "licenseId")}
    static var uploadLicenseFront: String { return Localization.localizedString(key, stringValue: "uploadLicenseFront")}
    static var uploadLicenseBack: String { return Localization.localizedString(key, stringValue: "uploadLicenseBack")}
    static var chasisNumber: String { return Localization.localizedString(key, stringValue: "chasisNumber")}
    static var engineNumber: String { return Localization.localizedString(key, stringValue: "engineNumber")}
    static var vehicleModel: String { return Localization.localizedString(key, stringValue: "vehicleModel")}
    static var vehicleMade: String { return Localization.localizedString(key, stringValue: "vehicleMade")}
    static var VehicleYear: String { return Localization.localizedString(key, stringValue: "VehicleYear")}
    static var vehicleColor: String { return Localization.localizedString(key, stringValue: "vehicleColor")}
    static var carFront: String { return Localization.localizedString(key, stringValue: "carFront")}
    static var carBack: String { return Localization.localizedString(key, stringValue: "carBack")}
    static var carSide: String { return Localization.localizedString(key, stringValue: "carSide")}
    static var required: String { return Localization.localizedString(key, stringValue: "required")}
    static var soon: String { return Localization.localizedString(key, stringValue: "soon")}
    static var forgotPasswordDetails: String { return Localization.localizedString(key, stringValue: "forgotPasswordDetails")}
    static var guest: String { return Localization.localizedString(key, stringValue: "guest")}
    static var myinfo: String { return Localization.localizedString(key, stringValue: "myinfo")}
    static var saved: String { return Localization.localizedString(key, stringValue: "saved")}
    static var account: String { return Localization.localizedString(key, stringValue: "account")}
    static var sociallogin: String { return Localization.localizedString(key, stringValue: "sociallogin")}
    static var forgotdetails: String { return Localization.localizedString(key, stringValue: "forgotdetails")}
    static var verifydetails: String { return Localization.localizedString(key, stringValue: "verifydetails")}
    static var birthday: String { return Localization.localizedString(key, stringValue: "birthday")}
    static var birthdayrequired: String { return Localization.localizedString(key, stringValue: "birthdayrequired")}
    static var messagerequired: String { return Localization.localizedString(key, stringValue: "messagerequired")}
    static var topic: String { return Localization.localizedString(key, stringValue: "topic")}
    static var topicrequird: String { return Localization.localizedString(key, stringValue: "topicrequird")}
    static var nationIdrequired: String { return Localization.localizedString(key, stringValue: "nationIdrequired")}
    static var boxid: String { return Localization.localizedString(key, stringValue: "boxid")}
    static var boxidrequired: String { return Localization.localizedString(key, stringValue: "boxidrequired")}
    static var nodata: String { return Localization.localizedString(key, stringValue: "nodata")}
    static var profit: String { return Localization.localizedString(key, stringValue: "profit")}
    static var civilnumber: String { return Localization.localizedString(key, stringValue: "civilnumber")}
    static var workinghours: String { return Localization.localizedString(key, stringValue: "workinghours")}
    static var saturday: String { return Localization.localizedString(key, stringValue: "saturday")}
    static var sunday: String { return Localization.localizedString(key, stringValue: "sunday")}
    static var monday: String { return Localization.localizedString(key, stringValue: "monday")}
    static var tuesday: String { return Localization.localizedString(key, stringValue: "tuesday")}
    static var wenthday: String { return Localization.localizedString(key, stringValue: "wenthday")}
    static var thurthday: String { return Localization.localizedString(key, stringValue: "thurthday")}
    static var friday: String { return Localization.localizedString(key, stringValue: "friday")}
    static var karin: String { return Localization.localizedString(key, stringValue: "karin")}
    static var mubarak: String { return Localization.localizedString(key, stringValue: "mubarak")}
    static var days: String { return Localization.localizedString(key, stringValue: "days")}
    static var area: String { return Localization.localizedString(key, stringValue: "area")}
    static var day: String { return Localization.localizedString(key, stringValue: "day")}
    static var time: String { return Localization.localizedString(key, stringValue: "time")}
    static var locationerror: String { return Localization.localizedString(key, stringValue: "locationerror")}
    static var occation: String { return Localization.localizedString(key, stringValue: "occation")}
    static var details: String { return Localization.localizedString(key, stringValue: "details")}
    static var price: String { return Localization.localizedString(key, stringValue: "price")}
    static var discount: String { return Localization.localizedString(key, stringValue: "discount")}
}

struct HomeLocalizedString {
    private static let key = "Home"
    static var subscribeNow: String {return Localization.localizedString(key, stringValue: "subscribeNow") }
    static var Points: String {return Localization.localizedString(key, stringValue: "Points") }
    static var PointBack: String {return Localization.localizedString(key, stringValue: "PointBack") }
    static var mypoint: String {return Localization.localizedString(key, stringValue: "mypoint") }
    static var buynow: String {return Localization.localizedString(key, stringValue: "buynow") }
    static var Payment: String {return Localization.localizedString(key, stringValue: "Payment") }
    static var copetCondition: String {return Localization.localizedString(key, stringValue: "copetCondition") }
    static var coptDes: String {return Localization.localizedString(key, stringValue: "coptDes") }
    static var questions: String {return Localization.localizedString(key, stringValue: "questions") }
    static var question: String {return Localization.localizedString(key, stringValue: "question") }
}
struct TabBarLocalizedString {
    private static let key = "TabBar"
    static var socialactivities: String {return Localization.localizedString(key, stringValue: "socialactivities") }
    static var salesoffers: String {return Localization.localizedString(key, stringValue: "salesoffers") }
    static var news: String {return Localization.localizedString(key, stringValue: "news") }
    static var Diwaniyas: String {return Localization.localizedString(key, stringValue: "Diwaniyas") }
    static var callus: String {return Localization.localizedString(key, stringValue: "callus") }
    static var aboutus: String {return Localization.localizedString(key, stringValue: "aboutus") }
    static var branchesdirectory: String {return Localization.localizedString(key, stringValue: "branchesdirectory") }
    static var shoppinglist: String {return Localization.localizedString(key, stringValue: "shoppinglist") }
    static var barcode: String {return Localization.localizedString(key, stringValue: "barcode") }
    static var Investors: String {return Localization.localizedString(key, stringValue: "Investors") }
    static var areafacilities: String {return Localization.localizedString(key, stringValue: "areafacilities") }
    static var Shareholdersprofits: String {return Localization.localizedString(key, stringValue: "Shareholders'profits") }
    static var Occasions: String {return Localization.localizedString(key, stringValue: "Occasions") }
    static var Twitter: String {return Localization.localizedString(key, stringValue: "Twitter") }
    static var Instagram: String {return Localization.localizedString(key, stringValue: "Instagram") }
    static var Gallery: String {return Localization.localizedString(key, stringValue: "Gallery") }
    static var Email: String {return Localization.localizedString(key, stringValue: "Email") }
    static var Facebook: String {return Localization.localizedString(key, stringValue: "Facebook") }
    static var investbranches: String {return Localization.localizedString(key, stringValue: "investbranches") }
    static var service: String {return Localization.localizedString(key, stringValue: "service") }
    static var complain: String {return Localization.localizedString(key, stringValue: "complain") }
    static var invalidlink: String {return Localization.localizedString(key, stringValue: "invalidlink") }
    static var whatsappcontact: String {return Localization.localizedString(key, stringValue: "whatsappcontact") }
    static var phonecontact: String {return Localization.localizedString(key, stringValue: "phonecontact") }
    static var emailcontact: String {return Localization.localizedString(key, stringValue: "emailcontact") }
    static var gamaianame: String {return Localization.localizedString(key, stringValue: "gamaianame") }
    static var diwaniyarecord: String {return Localization.localizedString(key, stringValue: "diwaniyarecord") }
    static var reserve: String {return Localization.localizedString(key, stringValue: "reserve") }
    static var mycivil: String {return Localization.localizedString(key, stringValue: "mycivil") }
    
}

