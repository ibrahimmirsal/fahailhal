//
//  APPLanguageHandler.swift
//  Mirsal_Swift
//
//  Created by Ibrahim on 1/6/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation

class AppLanguageHandler {
    
    static var object:AppLanguageHandler? = nil
    static func getObject() -> AppLanguageHandler {
        if (AppLanguageHandler.object == nil) {
            AppLanguageHandler.object = AppLanguageHandler()
        }
        return AppLanguageHandler.object!
    }
    
//    func getLanguageDic() -> NSDictionary {
//        if (UserDefaults.standard.string(forKey: AppConstant.getObject().LANGUAGE_KEY) == AppConstant.getObject().ARABIC) {
//            let path = Bundle.main.path(forResource:"ArabicList", ofType: "plist")
//            return NSDictionary(contentsOfFile: path!)!
//        } else {
//            let path = Bundle.main.path(forResource:"EnglishList", ofType: "plist")
//            return NSDictionary(contentsOfFile: path!)!
//        }
//    }
//    
//    func getStringForKey(key:String) -> String {
//        let dic = getLanguageDic()
//        return dic.object(forKey: key) as! String
//    }
//    
    func getLanguage() -> String {
        if (UserDefaults.standard.string(forKey: AppConstant.getObject().LANGUAGE_KEY) == AppConstant.getObject().ARABIC) {
            return AppConstant.getObject().ARABIC
        } else {
            return AppConstant.getObject().ENGLISH
        }
    }
    
    func IsEnglish() -> Bool {
        if (UserDefaults.standard.string(forKey: AppConstant.getObject().LANGUAGE_KEY) == AppConstant.getObject().ARABIC) {
            return false
        } else {
            return true
        }
    }
    
    func getMyQR() -> String {
        return UserDefaults.standard.string(forKey: AppConstant.getObject().MYQR_KEY) ?? ""
    }
    
    func saveMyQR(_ qr:String) {
        UserDefaults.standard.set(qr, forKey: AppConstant.getObject().MYQR_KEY)

    }
}
