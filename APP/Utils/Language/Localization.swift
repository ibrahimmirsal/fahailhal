
import Foundation
class Localization {
    class func localizedString(_ key: String, stringValue: String) -> String {
        if let path = Bundle.main.url(forResource: AppLanguageHandler.getObject().getLanguage(), withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = object as? [String: Any] {
                    let dictValue = dictionary[key] as! [String: String]
                    let value = dictValue[stringValue]
                    return value ?? ""
                }
                return ""
            }
            catch {
                return ""
            }
        }
        return ""
      
    }
 
    
    private class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                //  print(error.localizedDescription)
            }
        }
        return nil
    }
}
