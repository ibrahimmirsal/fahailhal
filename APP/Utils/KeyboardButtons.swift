//
//  KeyboardButtons.swift
//  WassalnyDriver
//
//  Created by Mahmoud Maamoun on 9/17/18.
//  Copyright © 2018 Mahmoud Maamoun. All rights reserved.
//

import Foundation
import UIKit
class APPKeyboardButtons {
    static var object:APPKeyboardButtons? = nil
    static func getObject() -> APPKeyboardButtons {
        if (APPKeyboardButtons.object == nil) {
            APPKeyboardButtons.object = APPKeyboardButtons()
        }
        return APPKeyboardButtons.object!
    }
    
    var button_tag = 0
    var containerVC = UIViewController()
    var max_tag = 0
    var min_tag = 0
    let constant = AppConstant.getObject()
    let langugagweHandler = AppLanguageHandler.getObject()
    
    func getKeboardButtons (textfield : UITextField , container : UIViewController) {
        containerVC = container
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.barTintColor = UIColor.lightGray
        let next = AlertLocalizedString.next
        let previos = AlertLocalizedString.previous
        let  prev_keyboard_button =  UIBarButtonItem(title: previos, style: .plain, target: self, action: #selector(addTappedPrev))
        let  next_keyboard_button =  UIBarButtonItem(title: next, style: .plain, target: self, action: #selector(addTappedNect))
        let done = UIBarButtonItem(title: AlertLocalizedString.done, style: .plain, target: self, action: #selector(addTappedDone))
        if textfield.tag == max_tag {
            numberToolbar.items = [
                prev_keyboard_button,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),done]
        }else {
            numberToolbar.items = [
                prev_keyboard_button,
                next_keyboard_button,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)]
        }
        
        numberToolbar.sizeToFit()
        // bind vars
        textfield.inputAccessoryView = numberToolbar
    }
    
    func getKeboardButtonsForTextView (textfield : UITextView , container : UIViewController) {
        containerVC = container
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.barTintColor = UIColor.lightGray
        let done = UIBarButtonItem(title: AlertLocalizedString.done, style: .plain, target: self, action: #selector(addTappedDone))
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),done]
        numberToolbar.sizeToFit()
        // bind vars
        textfield.inputAccessoryView = numberToolbar
    }
    func getKeboardButtonsForPicker (textfield : UITextField , container : UIViewController) {
        containerVC = container
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.barTintColor = UIColor.lightGray
        let done = UIBarButtonItem(title: AlertLocalizedString.done, style: .plain, target: self, action: #selector(addTappedDone))
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),done]
        numberToolbar.sizeToFit()
        // bind vars
        textfield.inputAccessoryView = numberToolbar
    }
    
    @objc func addTappedDone() {
      //  containerVC.view.endEditing(true)
    }
    
    @objc func addTappedPrev() {
        if button_tag <= max_tag {
//            if let input = containerVC.view.viewWithTag(button_tag - 1) as? UITextField {
//                input.becomeFirstResponder()
//            }
        }
    }
    
    @objc func addTappedNect() {
        if button_tag >= min_tag {
//            if let input = containerVC.view.viewWithTag(button_tag + 1) as? UITextField {
//                input.becomeFirstResponder()
//            }
        }
    }
    
    func getDoneButton (textfield : UITextField , container : UIViewController) {
        containerVC = container
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.barTintColor = UIColor.lightGray
        let done = UIBarButtonItem(title: AlertLocalizedString.done, style: .plain, target: self, action: #selector(addTappedDone))
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),done]
        numberToolbar.sizeToFit()
        // bind vars
        textfield.inputAccessoryView = numberToolbar
    }
    
}
