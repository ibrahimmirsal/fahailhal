//
//  AppPopUpHandler.swift
//  Mirsal_Swift
//
//  Created by Afnan on 1/10/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import PopupDialog

class AppPopUpHandler {
    
    static var object:AppPopUpHandler? = nil
    
    static func getObject() -> AppPopUpHandler {
        
        if (AppPopUpHandler.object == nil) {
            AppPopUpHandler.object = AppPopUpHandler()
        }
        return AppPopUpHandler.object!
    }
    func showError (messageStr : String) {
        let vc = MissingPopUpViewController()
        vc.message = messageStr
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        getPopUpView(VC: vc, hight: (window?.rootViewController)!.view.bounds.height * 0.25 , dismissGesture: true)
    }
    func getPopUpView(VC: UIViewController ,hight:CGFloat , dismissGesture: Bool) {
        let window = (UIApplication.shared.delegate as! AppDelegate).window
        let popup = PopupDialog(viewController: VC, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: dismissGesture)
        popup.viewController.view.heightAnchor.constraint(equalToConstant: hight).isActive = true
        (window?.rootViewController)!.present(popup, animated: true, completion: nil)
    }
    
}
