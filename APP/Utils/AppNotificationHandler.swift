//
//  File.swift
//  APP
//
//  Created by mac on 12/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.

import Foundation
import RNNotificationView
import ObjectMapper
class AppNotificationHandler {
    static var object:AppNotificationHandler? = nil
    static func getObject() -> AppNotificationHandler {
        if (AppNotificationHandler.object == nil) {
            AppNotificationHandler.object = AppNotificationHandler()
        }
        return AppNotificationHandler.object!
    }
    var userInfo = [NSObject : AnyObject]()
    var comesFromNotifivication = false
    var homeOpen = false
    var socketMessage = false
    var messageSender = ""
    var message = ""
    private func presentChat() {
        
    }
    private func openOffers() {
        NotificationCenter.default.post(name: Notification.Name("openOffers"), object: nil)
    }
    private func openNews() {
        NotificationCenter.default.post(name: Notification.Name("openNews"), object: nil)
    }
    private func openActivities() {
        NotificationCenter.default.post(name: Notification.Name("openActivities"), object: nil)
    }
    private func openUrl() {
        NotificationCenter.default.post(name: Notification.Name("openUrl"), object: nil)
    }
    func parseRemoteNotification (userInfoData:[NSObject : AnyObject]){
        let messageDict = userInfoData as? Dictionary<String, Any>
        print(messageDict)
        guard let model = Mapper<NotificationsResponseModel>().map(JSON: messageDict!) else {
            return
        }
        var type = model.gcmnotificationtype
        
        guard let type2 = messageDict?["gcm.notification.type"] as? String  else {
            return
        }
        let title = model.aps.alert.title
        let body = model.aps.alert.body
        if type.count == 0 {
            type = type2
        }
        switch type {
            
        case "news":
            if comesFromNotifivication {
                self.openNews()
                comesFromNotifivication = false
                userInfo.removeAll()
            }else {
                let notification = RNNotificationView()
                notification.show(withImage: UIImage.init(named: "LOGO"), title:title,message:"\(body)" ,duration: 10, iconSize: CGSize(width: 22 , height: 22),onTap: {
                    //  print("Did tap notification")
                    self.openNews()
                })
            }
            
          
        break
        case "activities":
            if comesFromNotifivication {
                self.openActivities()
                comesFromNotifivication = false
                userInfo.removeAll()
            }else {
                let notification = RNNotificationView()
                notification.show(withImage: UIImage.init(named: "LOGO"), title:title,message:"\(body)" ,duration: 10, iconSize: CGSize(width: 22 , height: 22),onTap: {
                    //  print("Did tap notification")
                    self.openActivities()
                })
            }

        break
        case "offers":
            if comesFromNotifivication {
                self.openOffers()
                comesFromNotifivication = false
                userInfo.removeAll()
            }else {
                let notification = RNNotificationView()
                notification.show(withImage: UIImage.init(named: "LOGO"), title:title,message:"\(body)" ,duration: 10, iconSize: CGSize(width: 22 , height: 22),onTap: {
                    //  print("Did tap notification")
                    self.openOffers()
                })
            }
        break
            
        case "url":
            UserDefaults.standard.set(body, forKey: "notifurl")
            if comesFromNotifivication {
                comesFromNotifivication = false
                userInfo.removeAll()
                self.openUrl()
            }else {
                let notification = RNNotificationView()
                notification.show(withImage: UIImage.init(named: "LOGO"), title:title,message:"\(body)" ,duration: 10, iconSize: CGSize(width: 22 , height: 22),onTap: {
                    //  print("Did tap notification")
                    self.openUrl()
                })
            }
        break
         
        default: break
        }
        
    }
}
class NotificationsResponseModel : Mappable {
   var gcmnotificationtype  = ""
    var googlecae = ""
    var gcmnotificationitem_id = ""
    var gcmmessage_id = ""
    var aps = AppModel()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        aps <- map["aps"]
        gcmnotificationtype <- map["gcm.notification.type"]
        googlecae <- map["google.c.a.e"]
        gcmmessage_id <- map["gcm.message_id"]
        gcmnotificationitem_id <- map["gcm.notification.item_id"]
        
    }
}
class AppModel : Mappable {
   var alert  = AlertModel()
    var sound = ""
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        alert <- map["alert"]
        sound <- map["sound"]
    }
}
class AlertModel : Mappable {
    var body  = ""
    var title = ""
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        body <- map["body"]
        title <- map["title"]
    }
}

