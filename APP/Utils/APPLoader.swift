//
//  Loader.swift
//  Mirsal_Swift
//
//  Created by Afnan on 1/13/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import UIKit
import MBProgressHUD

class APPLoader {
    
    static var object:APPLoader? = nil
    static func getObject() -> APPLoader {
        if (APPLoader.object == nil) {
            APPLoader.object = APPLoader()
        }
        return APPLoader.object!
    }
    var hud: MBProgressHUD!
    
    func show(_ view: UIView, _ isAnimated: Bool) {
        hud = MBProgressHUD.showAdded(to: view, animated: isAnimated)
    }
    
    func showWithText(_ view :UIView , _ isAnimated : Bool , text : String ) {
        hud = MBProgressHUD.showAdded(to: view, animated: isAnimated)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = text
    }
    
    func hide() {
        DispatchQueue.global(qos: .userInitiated).async {
            // Do something...
            DispatchQueue.main.async {
                APPLoader.getObject().hud.hide(animated: true)
            }
        }
    }
    
}
