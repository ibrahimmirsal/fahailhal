//
//  APPConstants.swift
//  Mirsal_Swift
//
//  Created by Ibrahim on 1/6/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper

class AppConstant {
    static var object:AppConstant? = nil
    static func getObject() -> AppConstant {
        if (AppConstant.object == nil) {
            AppConstant.object = AppConstant()
        }
        return AppConstant.object!
    }
    var controllerTitle = ""
    func getCurrentViewControllerTitle ()-> String {
        return controllerTitle
    }
    var TAB_INDEX = 0
    let MYQR_KEY = "my_qr"
    let LANGUAGE_KEY = "lang"
    let ACOUNT = "acount"
    let PHONE_LENGTH = 11
    let DEVICE_TOKEN_KEY  = "device_token"
    let USER_TOKEN_KEY = "token"
    let ACCESS_TOKEN = "access_token"
    let USER_ID = "user_id"
    let WHATSAPP_LINK = "https://api.whatsapp.com/send?phone=96522243200"
    let FACEBOOK_LINK = "fb://profile/1578301132495633"
    let TWITTER_LINK = "twitter://user?screen_name=mirsalapp"
    let INSTGRAM_LINK = "instagram://user?username=mirsalapp"
    let BECOME_MIRSAL = "https://docs.google.com/forms/d/e/1FAIpQLSe1BZbuwCWYW_Zkz_UU8Bff5nEtjVQ6nCBl86_JCklHbOcX_A/viewform"
    let ARABIC = "ar"
    let ENGLISH = "en"
    let FONT_AR_Bold_CUS = "GESSTwoMedium-Medium"
    let FONT_AR_LIGHT_CUS = "GESSTwoLight-Light"
    let FONT_EN_Bold_CUS = "TitilliumWeb-SemiBold"
    let FONT_EN_LIGHT_CUS = "TitilliumWeb-Regular"
    let FONT_AR_Bold = "System Font Bold"
    let FONT_AR_LIGHT = "System Font Regular"
    let FONT_EN_Bold = "System Font Bold"
    let FONT_EN_LIGHT = "System Font Regular"
    let LIGHT_GRAY_COLOR = "#DDDDDD"
    let GRAY_COLOR = "#808080"
    let color1 = "#007AA0"
    let color2 = "#005A96"
    let color3 = "#007DAA"
    let BLUE_COLOR_LIGHT = "#5663FF"
    let BLUE_COLOR = "#254A87"
    let RED_COLOR = "#BA1E42"
    let TEXT_COLOR = "#3E3F68"
    let LINE_COLOR = "#685F86"
    let BACKGROUND_COLOR = "#EFF2FD"
    let IS_USER_LOGGED = "IS_USER_LOGGED"
    var DEVICE_TOKEN = ""
    let GOOGLE_API_KEY = "AIzaSyDNhV2PBw7dWzGFH2icGiiiQ9rFhIxE6X0"
}
