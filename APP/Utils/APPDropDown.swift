//
//  APPDropDown.swift
//  automative
//
//  Created by mac on 4/4/19.
//  Copyright © 2019 mac. All rights reserved.
//


import Foundation
import DropDown
protocol DropDownProtocol {
    func getItem(item:String , itemIndex : Int , type: String)
}
class APPDropDownHandler {
    let dropDown = DropDown()
    var dropDownDelegate : DropDownProtocol?
    static var object:APPDropDownHandler? = nil
    static func getObject() -> APPDropDownHandler {
        if (APPDropDownHandler.object == nil) {
            APPDropDownHandler.object = APPDropDownHandler()
        }
        return APPDropDownHandler.object!
    }
    func setDropDown(arr:[String] ,view:UIView, type:String)  {
        DropDown.startListeningToKeyboard()
        dropDown.anchorView = view
        dropDown.dataSource = arr 
        dropDown.direction = .top
        dropDown.width = view.frame.size.width
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dropDownDelegate?.getItem(item: item, itemIndex: index, type: type)
            self.dropDown.hide()
        }
        show()
    }
    
    func show() {
        dropDown.show()
    }
}
