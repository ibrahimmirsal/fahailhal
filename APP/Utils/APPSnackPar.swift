//
//  APPSnackPar.swift
//  automative
//
//  Created by mac on 4/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import TTGSnackbar
class APPSnackbar {
    static var object:APPSnackbar? = nil
    static func getObject() -> APPSnackbar {
        if (APPSnackbar.object == nil) {
            APPSnackbar.object = APPSnackbar()
        }
        return APPSnackbar.object!
    }
    func showMessage(message:String) {
        TTGSnackbar.init(message:message, duration: .middle).show()
    }
}
