//
//  API_URLs.swift
//  Mirsal_Swift
//
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import UIKit
class API_URLs {
  //  let APPID = 67
    let APPID = 77
    let BASE_URL = "http://apps.semaphore.me/mobile/ws/"
    let TOKEN = "90491a429ea2b5510fc9856be47a6e24"
    static var object: API_URLs? = nil
    static func getObject() -> API_URLs {
        if (API_URLs.object == nil) {
            API_URLs.object = API_URLs()
        }
        return API_URLs.object!
    }
    func getNewsUrl() -> String {
        return "\(BASE_URL)news/get-news-list?app=\(APPID)&lang=ar&token=\(TOKEN)&type=news"
    }
    func getSliderURL () -> String {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let device_name = UIDevice.current.name
        let device_version = UIDevice.current.systemVersion
        let device_uid = UIDevice.current.identifierForVendor?.uuidString
        let device_type = "ios"
        let device_token = UserDefaults.standard.string(forKey: AppConstant.getObject().DEVICE_TOKEN) ?? ""
        return "http://apps.semaphore.me/mobile/ws/ads/photo/lang/ar/type/news/app/\(APPID)?token=\(TOKEN)&app_version=\(appVersion ?? "")&device_type=\(device_type)&device_name=\(device_name)&device_version=\(device_version)&device_uid=\(device_uid ?? "")&device_token=\(device_token)"
    }
    func getRegisterDeviceURL () -> String {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let device_name = UIDevice.current.name
        let device_version = UIDevice.current.systemVersion
        let device_uid = UIDevice.current.identifierForVendor?.uuidString
        let device_type = "ios"
        let device_token = UserDefaults.standard.string(forKey: AppConstant.getObject().DEVICE_TOKEN) ?? ""
        return "\(BASE_URL)notification/register-device?app=\(APPID)&token=\(TOKEN)&app_version=\(appVersion ?? "")&device_type=\(device_type)&device_name=\(device_name)&device_version=\(device_version)&device_uid=\(device_uid ?? "")&device_token=\(device_token)"
    }
    func getOffersURL() -> String {
        return "\(BASE_URL)news/get-news-list?app=\(APPID)&lang=ar&token=\(TOKEN)&type=offers"
    }
    func getActivitiesURL () -> String {
        return "\(BASE_URL)news/get-news-list?app=\(APPID)&lang=ar&token=\(TOKEN)&type=activities"
    }
    func getAboutURL () -> String {
        return "\(BASE_URL)cms/get-cms-details/lang/ar/app/\(APPID)/id/26?token=\(TOKEN)"
    }
    func getCallUSURL () -> String {
        return "\(BASE_URL)cms/get-cms-details/lang/ar/app/\(APPID)/id/26?token=\(TOKEN)"
    }
    func getBranchesURL () -> String {
        return "\(BASE_URL)branches/get-branches?app=\(APPID)&lang=ar&token=\(TOKEN)&type=branches"
    }
    func getContactUSURL () -> String {
        return "http://portal.web4sms.net/iPhone/mubarakContact.php"
    }
    func getInvestBrachesURL () -> String {
        return "\(BASE_URL)branches/get-stores?app=\(APPID)&token=\(TOKEN)"
    }
    func getInvestDetail (nationalid:String , boxid:String) -> String{
        print(boxid)
        //    return "\(BASE_URL)share/get-year-profit?app=\(APPID)&lang=ar&box_id=\(boxid)&id=\(nationalid)&token=\(TOKEN)"
        return "\(BASE_URL)share/get-year-profit?app=\(APPID)&lang=ar&&id=\(nationalid)&token=\(TOKEN)"
    }
    func getDiwanisURL () -> String {
        return "\(BASE_URL)diwaniya/get-diwaniya-list?app=\(APPID)&token=\(TOKEN)"
    }
    func getDeathOccasionURL () -> String {
        return "\(BASE_URL)notification/get-notify-list?app=\(APPID)&token=\(TOKEN)&type=Death"
    }
    func getMarriageOccasionURL () -> String {
        return "\(BASE_URL)notification/get-notify-list?app=\(APPID)&token=\(TOKEN)&type=Marriage"
    }
    func getBarCodeProductsURL (code:String) -> String{
        return "\(BASE_URL)products/get-products/lang/ar/barcode/\(code)/app/\(APPID)?token=\(TOKEN)"
    }
}

