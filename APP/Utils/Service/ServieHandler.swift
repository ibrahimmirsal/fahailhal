//
//  ServieHandler.swift
//  Mirsal_Swift
//
//  Created by Ibrahim on 1/8/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHandler {
    
    static let sharedInstance = ServiceHandler()
    
    private func checkConnection() -> Bool {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (reachabilityManager?.isReachable)!
    }
    
    func executeGetServiceWith(url:String, successBlock: @escaping (_ response:[String: Any]) -> Void,
                               errorBlock: @escaping (_ errorResponse:String) -> Void)  {
        if checkConnection() {
            var head: HTTPHeaders = [:]
            if (UserDefaults.standard.object(forKey: AppConstant.getObject().ACCESS_TOKEN) != nil) {
                head = ["Authorization":"Bearer \(UserDefaults.standard.object(forKey: AppConstant.getObject().ACCESS_TOKEN) ?? "")","Content-Type":"application/json"]
                
            }
            print(url)
            let encodeUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            Alamofire.request(encodeUrl!, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: head.count == 0 ? nil : head).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if let jsonDict = response.result.value as? Dictionary<String, Any> {
                        successBlock(jsonDict)
                    }
                    break
                case .failure(let error):
                    if !(self.checkConnection()) {
                        errorBlock(AlertLocalizedString.error_connection)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print(error.localizedDescription)
                        errorBlock(AlertLocalizedString.error_connection)
                    }else {
                         errorBlock(error.localizedDescription)
                    }
                }
            }
        } else {
            errorBlock(AlertLocalizedString.error_connection)
            
        }
    }
    
    func executePostServiceWith(url:String,params:[String:Any], successBlock: @escaping (_ response:[String: Any]) -> Void,
                                errorBlock: @escaping (_ errorResponse:String) -> Void)  {
        if checkConnection() {
            var head: HTTPHeaders = [:]
            if (UserDefaults.standard.object(forKey: AppConstant.getObject().ACCESS_TOKEN) != nil) {
            head = ["Authorization":"Bearer \(UserDefaults.standard.object(forKey: AppConstant.getObject().ACCESS_TOKEN) ?? "")","Content-Type":"application/json"]
            }
            
            Alamofire.request(url,method: .post,parameters: params.count == 0 ? nil : params,encoding: JSONEncoding.default,headers:head.count == 0 ? nil : head).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                   if let jsonDict = response.result.value as? Dictionary<String, Any> {
                        successBlock(jsonDict)
                    }

                    break
                case .failure(let error):
                    if !(self.checkConnection()) {
                        errorBlock(AlertLocalizedString.error_connection)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print(error.localizedDescription)
                        errorBlock(AlertLocalizedString.error_connection)
                    }else {
                        errorBlock(error.localizedDescription)
                    }
                }
            }
        } else {
            errorBlock(AlertLocalizedString.error_connection)
            
        }
    }
    func handle(dicc: [String: Any]) -> [String: Any] {
        var dic = dicc
        for (key, value) in dic {
            if value is NSNull {
                dic[key] = "" as Any
            } else if value is Int {
                let temp : Int =  value as! Int
                dic[key] = String(temp)
            } else if value is Double {
                let temp : Double = value as! Double
                dic[key] = String(temp)
            }
        }
        return dic
    }
    
}
