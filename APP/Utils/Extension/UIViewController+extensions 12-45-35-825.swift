//  UIView+Extensions.swift
//  PEEP Captain
//
//  Created by Ahmed Gamal on 7/26/19.
//  Copyright © 2019 ibrahim. All rights reserved.

import UIKit
//import SwiftMessages
let languageHandler = AppLanguageHandler.getObject()
extension UIViewController {
     func viewSlideInFromRight(toLeft views: UIView) {
        let kSlideAnimationDuration: CFTimeInterval = 0.4
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromRight
        //        transition?.delegate = (self as! CAAnimationDelegate)
        views.layer.add(transition!, forKey: nil)
    }
    
     func viewSlideInFromLeft(toRight views: UIView) {
        let kSlideAnimationDuration: CFTimeInterval = 0.4
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromLeft
        //        transition?.delegate = (self as! CAAnimationDelegate)
        views.layer.add(transition!, forKey: nil)
    }
    
     func viewSlideInFromTop(toBottom views: UIView) {
        let kSlideAnimationDuration: CFTimeInterval = 0.4
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromBottom
        //        transition?.delegate = (self as! CAAnimationDelegate)
        views.layer.add(transition!, forKey: nil)
    }
    
     func viewSlideInFromBottom(toTop views: UIView) {
        let kSlideAnimationDuration: CFTimeInterval = 0.4
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromTop
        //        transition?.delegate = (self as! CAAnimationDelegate)
        views.layer.add(transition!, forKey: nil)
    }
    func displayMessage(message : String, messageError : Bool) {
//        let view = MessageView.viewFromNib(layout: MessageView.Layout.messageView)
//        if messageError == true {
//            view.configureTheme(.error)
//        }else{
//            view.configureTheme(.success)
//        }
//        view.iconImageView?.isHidden = true
//        view.iconLabel?.isHidden = true
//        view.titleLabel?.isHidden = true
//        view.bodyLabel?.text = message
//        view.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        view.bodyLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        view.button?.isHidden = true
//        
//        var config = SwiftMessages.Config()
//        config.presentationStyle = .bottom
//        SwiftMessages.show(config: config, view: view)
    }
    
    
    func showError(_ errorMsg:String,_ title:String) {
        var text = ""
        if languageHandler.IsEnglish() {
            text = "Ok"
        }else{
            text = "حسنا"
        }
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: "\(errorMsg)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: text, style: UIAlertAction.Style.default, handler:nil))
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor.red
        }
        
    }
    func validateEmail(_ email:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    //  coustom allerts
    func alert(message: String){
        var text = ""
        if languageHandler.IsEnglish() {
            text = "Ok"
        }else{
            text = "حسنا"
        }
        
        let alertView = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: text, style: .default) { (action) in
            // pop here
        }
        alertView.addAction(OKAction)
        present(alertView, animated: true, completion: nil)
    }
    
    func validateAlphabetical(_ text: String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = text.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (text == output)
        print("\(isValid)")
        
        return isValid
    }
    
    func setMainBackground(_ image: String) {
        //        view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        //        view.contentMode = .scaleAspectFit
        //        view.clipsToBounds = true
        
        UIGraphicsBeginImageContext(view.frame.size)
        var image = UIImage(named: image)
        image?.draw(in: view.bounds)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image!)
    }
    
    func round(_ image: UIImageView){
        image.layer.cornerRadius = image.frame.width / 2
        image.layer.masksToBounds = true
    }
    
    func roundButton(_ button: UIButton){
        button.layer.cornerRadius = button.frame.width / 2
        button.layer.masksToBounds = true
    }
    
    func bottomBoarder(border: UITextField){
        
        let borderName = CALayer()
        let widthName = CGFloat(2.0)
        
        borderName.borderColor = UIColor.white.cgColor
        borderName.frame = CGRect(x: 0, y: border.frame.size.height - widthName, width:  border.frame.width , height: 0.5)
        borderName.borderWidth = widthName
        border.layer.addSublayer(borderName)
        border.layer.masksToBounds = true
        
    }
    func dismissMe(animated: Bool, completion: (()->())?) {
        var count = 0
        if let c = self.navigationController?.viewControllers.count {
            count = c
        }
        if count > 1 {
            self.navigationController?.popViewController(animated: animated)
            if let handler = completion {
                handler()
            }
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
            dismiss(animated: animated, completion: completion)
        }
    }
    func openURL (url:String){
        if let url = URL(string: "\(url)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }

    
}

