//
//  CustomClasses.swift
//  Notice
//
//  Created by Mahmoud Maamoun on 10/23/17.
//  Copyright © 2017 Mahmoud Maamoun. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

// UISlider
class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}
// UITableView
class RoundedTBView: UITableView {
    override func awakeFromNib() {
        self.layer.cornerRadius = 16.0
    }
}

// UIView

class RoundedView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        round(with: bounds.size.height / 2) //
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        round(with: bounds.size.height / 2)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        //  setBorder(width: 1, with: .lightGray)
        round(with: bounds.size.height / 2)
    }
    
}

extension UIView
{
    
    func round(with radius: CGFloat)  {
        self.layer.cornerRadius = radius
    }
    
    var midHeight: CGFloat {
        return bounds.size.height / 2
    }
    
    func setBorder(width: CGFloat, with color: UIColor) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
    
}


class AttachmentView: UIView {
    
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var deleteAttachment: UIButton!
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.layer.borderWidth = 2.0
        self.clipsToBounds = true
    }
}
class RoundedBoldView: UIView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = bounds.size.height / 2 //16.0
        self.layer.borderColor  = UIColor.black.withAlphaComponent(0.2).cgColor
        self.layer.borderWidth = 0.70
        self.clipsToBounds = true
        self.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    }
    
}
class BorderView: UIView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderColor  = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR).cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
    
}
class CornerView: UIView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        AppUtils.getObject().makeShadow(view: self)
    }
    
}
class BorderBlueView: UIView {
    
    override func awakeFromNib() {
        self.layer.borderColor  = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().BLUE_COLOR_LIGHT).cgColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
    }
    
}
class LineView: UIView {
    override func awakeFromNib() {
       self.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().BLUE_COLOR)
    }
}
class GradientView: UIView {
    override func awakeFromNib() {
        let layer = CAGradientLayer()
        layer.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height * 0.08)
        layer.startPoint = CGPoint(x: 0.0, y: 1.0)
        layer.endPoint = CGPoint(x: 1.0, y: 1.0)
        layer.colors = [AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().color3).cgColor , AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().BLUE_COLOR).cgColor]
//        layer.colors = [AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().NEW_COLOR).cgColor , AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().NEW_COLOR).cgColor]
        self.layer .insertSublayer(layer, at: 0)
    }
}
class ShadowView: UIView {
    override func awakeFromNib() {
        AppUtils.getObject().makeShadow(view: self)
    }
}
class RoundedCircluerView: UIView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.layer.frame.width / 2
        self.layer.borderColor  = UIColor.darkGray.cgColor
        self.layer.borderWidth = 3.5
        self.clipsToBounds = true
    }
}
class CustomScrollView: UIScrollView {
    override func awakeFromNib() {
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            self.isScrollEnabled = false
        }else {
            self.isScrollEnabled = true
        }
    }
}

// UIImageView
class RoundedImageView: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.bounds.size.width / 2
        //self.layer.borderColor  = UIColor.color("#eeeeee")?.cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
}
class RightImageView: UIImageView {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }else {
            
        }
    }
}
class LeftImageView: UIImageView {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if (AppLanguageHandler.getObject().IsEnglish()) {
        }else {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

class ImageViewFromUrl: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
      //  self.layer.borderWidth = 0.20
        self.contentMode = .center
        self.clipsToBounds = true
    }
    func showImage(url:String) {
        let main = URL(string: url)
        self.kf.indicator?.startAnimatingView()
        self.kf.setImage(with: main)
    }
}

// UIButton

class RoundedCircleBtn: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.layer.frame.size.height / 2
        self.clipsToBounds = true
        self.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR)
        self.titleLabel?.textColor = .white
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class RoundedBtn: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 10.0
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class RetryBtn: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().RED_COLOR)
        self.setTitleColor(.white, for: .normal)
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class RedBtn: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.backgroundColor = .white
        self.setTitleColor(AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().RED_COLOR), for: .normal)
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class BlackBtn: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 0
        self.backgroundColor = .white
        self.setTitleColor(AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().TEXT_COLOR), for: .normal)
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class BackBtn: UIButton {
    override func awakeFromNib() {
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
        //    self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        //    self.transform = CGAffineTransform(scaleX: 1, y: -1)
            self.rotate(179.1)
        }
    }
}
// UITextField
class CodeTextField: UITextField {
    override func awakeFromNib() {
        self.textColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().TEXT_COLOR)
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 0.8
        self.layer.borderColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LINE_COLOR).cgColor
        self.clipsToBounds = true
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .center
        }else {
            self.textAlignment = .center
            //   self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
    func setATTPlaceHolder (plcehoderString : String) {
        self.attributedPlaceholder = APPFonts.getObject().setAttributeFont(text: plcehoderString)
    }
}
class CenterTextField: UITextField {
    override func awakeFromNib() {
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .center
        }else {
            self.textAlignment = .center
            //   self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
    func setATTPlaceHolder (plcehoderString : String) {
        self.attributedPlaceholder = APPFonts.getObject().setAttributeFont(text: plcehoderString)
    }
}
class LeftTextField: UITextField {
    override func awakeFromNib() {
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .left
        }else {
            self.textAlignment = .right
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
    func setATTPlaceHolder (plcehoderString : String) {
        self.attributedPlaceholder = APPFonts.getObject().setAttributeFont(text: plcehoderString)
    }
}
class RightTextField: UITextField {
    override func awakeFromNib() {
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .right
        }else {
            self.textAlignment = .left
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
    func setATTPlaceHolder (plcehoderString : String) {
        self.attributedPlaceholder = APPFonts.getObject().setAttributeFont(text: plcehoderString)
    }
}

// UILabel
class RoundedLBL: UILabel {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.bounds.size.height / 2
        self.clipsToBounds = true
    }
}
class TitleLBL: UILabel {
    override func awakeFromNib() {
        self.textAlignment = .center
        self.font = APPFonts.getObject().setTitleFont(bold: true)
        self.clipsToBounds = true
        if (AppLanguageHandler.getObject().IsEnglish()) {
            
        }else {
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class LeftLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.adjustsFontSizeToFitWidth = true
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .left
        }else {
            self.textAlignment = .right
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class RightLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.adjustsFontSizeToFitWidth = true
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .right
        }else {
            self.textAlignment = .left
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class CenterLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        if (AppLanguageHandler.getObject().IsEnglish()) {
            self.textAlignment = .center
        }else {
            self.textAlignment = .center
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class ChatLabelLBL: UILabel {
    override func awakeFromNib() {
        self.layer.cornerRadius = 12
        self.clipsToBounds = true
    }
}
class PaddingLabel: UILabel {
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 10.0
    @IBInspectable var rightInset: CGFloat = 10.0
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
    }
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        let customRect = rect.inset(by: insets)
        super.drawText(in: customRect)
        //  super.drawText(in: UIEdgeInsetsInsetRect(rect, insets)
       // super.drawText(in: rect.UIEdgeInsetsInsetRect(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}















