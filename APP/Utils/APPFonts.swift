//
//  Utils.swift
//  WassalnyDriver
//
//  Created by Mahmoud Maamoun on 10/17/18.
//  Copyright © 2018 Mahmoud Maamoun. All rights reserved.
//

import Foundation
import UIKit

class APPFonts {
    static var object:APPFonts? = nil
    static func getObject() -> APPFonts {
        if (APPFonts.object == nil) {
            APPFonts.object = APPFonts()
        }
        return APPFonts.object!
    }
    let languageHandler = AppLanguageHandler.getObject()
    let constnt = AppConstant.getObject()
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    func setFont(bold:Bool) -> UIFont {
        var font : UIFont! = UIFont()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if languageHandler.IsEnglish() {
                if bold {
                    font = UIFont(name: constnt.FONT_EN_Bold, size: 15)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_EN_LIGHT, size: 15)!
                }
            }else {
                if bold {
                    font = UIFont(name: constnt.FONT_AR_Bold, size: 15)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_AR_LIGHT, size: 15)!
                }
            }
        }else {
            if languageHandler.IsEnglish() {
                if bold {
                    
                    font = UIFont(name: constnt.FONT_EN_Bold, size: 14)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_EN_LIGHT, size: 14)!
                }
            }else {
                if bold {
                    font = UIFont(name: constnt.FONT_AR_Bold, size: 14)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_AR_LIGHT, size: 14)!
                }
            }
        }
        return font
    }
    func setTitleFont(bold:Bool) -> UIFont {
        var font : UIFont! = UIFont()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if languageHandler.IsEnglish() {
                if bold {
                    font = UIFont(name: constnt.FONT_EN_Bold, size: 22)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_EN_LIGHT, size: 22)!
                }
            }else {
                if bold {
                    font = UIFont(name: constnt.FONT_AR_Bold, size: 22)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_AR_LIGHT, size: 22)!
                }
            }
        }else {
            if languageHandler.IsEnglish() {
                if bold {
                    
                    font = UIFont(name: constnt.FONT_EN_Bold, size: 20)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_EN_LIGHT, size: 20)!
                }
            }else {
                if bold {
                    font = UIFont(name: constnt.FONT_AR_Bold, size: 20)!
                    
                }else {
                    font = UIFont(name: constnt.FONT_AR_LIGHT, size: 20)!
                }
            }
        }
        return font
    }
    func setEnFont(bold:Bool) -> UIFont {
        var font : UIFont! = UIFont()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if bold {
                //font = UIFont(name: "Gilroy-Bold", size: 19)!
                font = UIFont(name: constnt.FONT_EN_Bold, size: 15)!
                
            }else {
                font = UIFont(name: constnt.FONT_EN_LIGHT, size: 15)!
            }
        }else {
            if bold {
                //  font = UIFont(name: "Gilroy-Bold", size: 15)!
                font = UIFont(name: constnt.FONT_EN_Bold, size: 14)!
                
            }else {
                font = UIFont(name: constnt.FONT_EN_LIGHT, size: 14)!
            }
            
        }
        return font
    }
    func setARFont(bold:Bool) -> UIFont {
        var font : UIFont! = UIFont()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if bold {
                font = UIFont(name: constnt.FONT_AR_Bold, size: 15)!
                
            }else {
                font = UIFont(name: constnt.FONT_AR_LIGHT, size: 15)!
            }
        }else {
            if bold {
                //   font = UIFont(name: "GESSTwoMedium-Medium", size: 15)!
                font = UIFont(name: constnt.FONT_AR_Bold, size: 14)!
                
            }else {
                font = UIFont(name: constnt.FONT_AR_LIGHT, size: 14)!
                //  font = UIFont(name: "GESSTwoMedium-Medium", size: 15)!
            }
            
        }
        return font
    }
    
    func setAttributeFont(text:String)-> NSAttributedString {
        var attStr = NSAttributedString()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if languageHandler.IsEnglish() {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_EN_LIGHT, size: 15)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }else {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_AR_LIGHT, size: 15)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }
        }else {
            if languageHandler.IsEnglish() {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_EN_LIGHT, size: 14)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }else {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_AR_LIGHT, size: 14)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }
        }
        return attStr
    }
    func setSmallAttributeFont(text:String)-> NSAttributedString {
        var attStr = NSAttributedString()
        let screen = UIScreen.main.bounds
        if screen.height >= 667 {
            if languageHandler.IsEnglish() {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_EN_LIGHT, size: 14)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }else {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_AR_LIGHT, size: 14)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }
        }else {
            if languageHandler.IsEnglish() {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_EN_LIGHT, size: 12)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }else {
                let attribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font : UIFont(name: constnt.FONT_AR_LIGHT, size: 12)!]
                attStr = NSAttributedString(string: text, attributes:attribute)
            }
        }
        return attStr
    }
    
}
