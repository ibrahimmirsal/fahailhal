//
//  Utils.swift
//  MirsalSeller
//
//  Created by Ibrahim on 3/11/19.
//  Copyright © 2019 Ibrahim. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import JavaScriptCore
//import ObjectMapper

class AppUtils {
    static var object:AppUtils? = nil
    var curruntViewController = UIViewController()
    var bookCommentHint = true
    var extractShowPopUp = true
    var updateView = false
    var audioPlayer = AVPlayer()
    static func getObject() -> AppUtils {
        if (AppUtils.object == nil) {
            AppUtils.object = AppUtils()
        }
        return AppUtils.object!
    }
   
    
    func roundView(view:UIView, round:Int) {
        view.layer.cornerRadius = CGFloat(round)
        view.clipsToBounds = true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:testStr)
    }
    func getDefaultParams () -> NSMutableDictionary {
        let dic  = NSMutableDictionary()
        dic.setValue("1", forKey: "device_type")
        dic.setValue("test", forKey: "os_type")
        dic.setValue("test", forKey: "device_model")
        dic.setValue("test", forKey: "mobile_release_version")
        dic.setValue("test", forKey: "mobile_build_version")
        dic.setValue(AppLanguageHandler.getObject().getLanguage(), forKey: AppConstant.getObject().LANGUAGE_KEY)
        if (UserDefaults.standard.value(forKey: AppConstant.getObject().DEVICE_TOKEN_KEY) == nil){
            dic.setValue("", forKey: AppConstant.getObject().DEVICE_TOKEN_KEY)
        }else {
            dic.setValue(UserDefaults.standard.value(forKey: AppConstant.getObject().DEVICE_TOKEN_KEY) as! String, forKey: AppConstant.getObject().DEVICE_TOKEN_KEY)
            
        }
        return dic
    }
    func isValidMobileNumber(mobil : String) -> Bool {
        if mobil.count >= 8{
            return true
        }else {
            return false
        }
    }
    func setPriceList(vegearr: NSMutableArray)  {
        let savedData = NSKeyedArchiver.archivedData(withRootObject: vegearr)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: "pricelist")
    }
    func getPriceList() -> NSMutableArray {
        let defaults = UserDefaults.standard
        var arr = NSMutableArray()
        if let savedArea = defaults.object(forKey: "pricelist") as? Data {
            arr = NSKeyedUnarchiver.unarchiveObject(with: savedArea) as! NSMutableArray
        }
        return arr
    }
    func setProductList(vegearr: NSMutableArray , key : String)  {
        let savedData = NSKeyedArchiver.archivedData(withRootObject: vegearr)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: key)
    }
    func getProductList(key : String) -> NSMutableArray {
        let defaults = UserDefaults.standard
        var arr = NSMutableArray()
        if let savedArea = defaults.object(forKey: key) as? Data {
            arr = NSKeyedUnarchiver.unarchiveObject(with: savedArea) as! NSMutableArray
        }
        return arr
    }
    func setMainProductList(vegearr: NSMutableArray)  {
        let savedData = NSKeyedArchiver.archivedData(withRootObject: vegearr)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: "mainProduct")
    }
    func getMainProductList() -> NSMutableArray {
        let defaults = UserDefaults.standard
        var arr = NSMutableArray()
        if let savedArea = defaults.object(forKey: "mainProduct") as? Data {
            arr = NSKeyedUnarchiver.unarchiveObject(with: savedArea) as! NSMutableArray
        }
        return arr
    }
    func isDeviceiPad() -> Bool {
        return (UIDevice.current.userInterfaceIdiom == .phone) ? false : true
    }
    
//    func setVisitedAreas(areas: NSMutableArray)  {
//        let savedData = NSKeyedArchiver.archivedData(withRootObject: areas)
//        let defaults = UserDefaults.standard
//        defaults.set(savedData, forKey: "visited_area")
//    }
//    func getVisitedAreas() -> NSMutableArray {
//        let defaults = UserDefaults.standard
//        var arr = NSMutableArray()
//        if let savedArea = defaults.object(forKey: "visited_area") as? Data {
//            arr = NSKeyedUnarchiver.unarchiveObject(with: savedArea) as! NSMutableArray
//        }
//        return arr
//    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func getDateFromTimstamp(str:String) -> String {
        let date = Date(timeIntervalSince1970: Double(str)!)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func getNumberFormat(number:String) -> String {
        var someString = number
        if someString == "" {
            someString = "0"
        }
        let myInteger = Double(someString)
        let myNumber = NSNumber(value:myInteger!)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from:myNumber)
        return formattedNumber!
    }
    
    func getThumbnial(bigImage:UIImage) -> UIImage {
        var width:CGFloat = 0.0
        var height:CGFloat = 0.0
        if bigImage.size.width > 1024 {
            width = 1024
            let ratio = bigImage.size.width / width
            height = bigImage.size.height / ratio
        } else {
            width = bigImage.size.width
            height = bigImage.size.height
        }
        let originalImage = bigImage
        let destinationSize = CGSize.init(width: width, height: height)
        UIGraphicsBeginImageContext(destinationSize)
        originalImage.draw(in: CGRect.init(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func addBorderToBtn(button:UIButton) {
        button.layer.cornerRadius = 8
        button.layer.borderColor = (UIColor .lightGray).cgColor
        button.layer.borderWidth = 0.5
    }
    
    func validateNumber(str: String) -> String {
        var dotCounter = 0
        var string = str
        string  = string.replacingOccurrences(of: ",", with: "")
        for s in string {
            if s == "٠" {
                string.removeLast()
                string.append("0")
            } else if s == "١" {
                string.removeLast()
                string.append("1")
            } else if s == "٢" {
                string.removeLast()
                string.append("2")
            } else if s == "٣" {
                string.removeLast()
                string.append("3")
            } else if s == "٤" {
                string.removeLast()
                string.append("4")
            } else if s == "٥" {
                string.removeLast()
                string.append("5")
            } else if s == "٦" {
                string.removeLast()
                string.append("6")
            } else if s == "٧" {
                string.removeLast()
                string.append("7")
            } else if s == "٨" {
                string.removeLast()
                string.append("8")
            } else if s == "٩" {
                string.removeLast()
                string.append("9")
            } else if s != "0" && s != "1" && s != "2" && s != "3" && s != "4" && s != "5" && s != "6" && s != "7" && s != "8" && s != "9" && s != "." {
                string.removeLast()
            } else if s == "." {
                dotCounter += 1
            }
        }
        if dotCounter > 1 {
            string.removeLast()
        }
        //        string = getNumberFormat(number: string)
        return string
    }
    
   
    func getYears () -> NSMutableArray {
        let years = NSMutableArray()
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let year =  components.year
        var index = -1
        while index < 30 {
            let result = year! - index
            years .add("\(result)")
            index = index + 1
        }
        return years
    }
    func stringFromHtml(htmlString: String) -> NSAttributedString? {
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        return attributedString
    }
//    func getSelectTextAsHtml (webView:UIWebView) -> String {
//        let selectedText = webView.stringByEvaluatingJavaScript(from: "(function(){var html = \"\";" +
//            "    if (typeof window.getSelection != \"undefined\") {" +
//            "        var sel = window.getSelection();" +
//            "        if (sel.rangeCount) {" +
//            "            var container = document.createElement(\"div\");" +
//            "            for (var i = 0, len = sel.rangeCount; i < len; ++i) {" +
//            "                container.appendChild(sel.getRangeAt(i).cloneContents());" +
//            "            }" +
//            "            html = container.innerHTML.toString();" +
//            "        }" +
//            "    } else if (typeof document.selection != \"undefined\") {" +
//            "        if (document.selection.type == \"Text\") {" +
//            "            html = document.selection.createRange().htmlText.toString();" +
//            "        }" +
//            "    }" +
//            "    return html.toString();})()")
//        let test = String(selectedText!.filter { !"\n".contains($0) })
//        return test
//    }
    func makeShadow (view:UIView){
        view.layer.shadowColor = hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR).cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 7
        view.layer.cornerRadius = 5
        view.layer.borderColor = UIColor.clear.cgColor
    }
   
}
