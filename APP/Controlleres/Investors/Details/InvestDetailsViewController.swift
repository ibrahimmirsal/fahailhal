//
//  InvestDetailsViewController.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class InvestDetailsViewController: UIViewController {
    @IBOutlet weak var labelProfitValue: UILabel!
    @IBOutlet weak var labelProfit: UILabel!
    @IBOutlet weak var labelBoxNumberValue: UILabel!
    @IBOutlet weak var labelBoxNumber: UILabel!
    @IBOutlet weak var labelCivilNumberValue: UILabel!
    @IBOutlet weak var labelCivilNumber: UILabel!
    @IBOutlet weak var labelNameValue: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var lblYearVal: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    var profitArray = [InvestorProfitModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initViews () {
        if profitArray.count > 0 {
            let model = profitArray[0]
            labelNameValue.text = "\(model.share_holder_name)"
            labelCivilNumberValue.text = "\(model.civil_id_number)"
            labelBoxNumberValue.text = "\(model.box_number)"
            labelProfitValue.text = "\(model.share_profit)"
            lblYearVal.text = model.year
        }
        labelName.text = LoginLocalizedString.name
        labelBoxNumber.text = LoginLocalizedString.boxid
        labelCivilNumber.text = LoginLocalizedString.civilnumber
        labelProfit.text = LoginLocalizedString.profit
        lblYear.text = LoginLocalizedString.year
    }
}
