//
//  InvestorsViewController.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class InvestorsViewController: UIViewController {

    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var textFieldBoxID: LeftTextField!
    @IBOutlet weak var labelBoxID: UILabel!
    @IBOutlet weak var textFieldID: LeftTextField!
    @IBOutlet weak var labelID: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    let presenter = HomePresenter()
    let indicator = APPLoader.getObject()
    var isQr: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func send(_ sender: Any) {
        self.view.endEditing(true)
        indicator.show(self.view, true)
        presenter.getIvestorDetails(nationlid: textFieldID.text!, boxid: textFieldBoxID.text!, successBlock: { (model) in
            self.indicator.hide()
            if self.isQr {
                languageHandler.saveMyQR(self.textFieldID.text ?? "")
                self.navigationController?.popViewController(animated: true)
                return
            }
            if model.profit.count > 0 {
                if self.isQr {
                    languageHandler.saveMyQR(self.textFieldID.text ?? "")
                    self.navigationController?.popViewController(animated: true)
                }else {
                    let vc = InvestDetailsViewController()
                    vc.profitArray = model.profit
                    self.navigationController?.pushViewController(vc, animated: true)
                }
               
            }else {
                APPSnackbar.getObject().showMessage(message: LoginLocalizedString.nodata)
            }
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
    func initView () {
        labelTitle.text = TabBarLocalizedString.Shareholdersprofits
        labelID.text = LoginLocalizedString.civilnumber
        labelBoxID.text = LoginLocalizedString.boxid
        btnSend.setTitle(LoginLocalizedString.send, for: .normal)
    }
    
}
