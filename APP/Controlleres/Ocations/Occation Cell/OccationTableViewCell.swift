//
//  OccationTableViewCell.swift
//  APP
//
//  Created by mac on 11/13/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class OccationTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: LeftLBL!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func drawOccationCell(model:OccationModel) {
        labelTitle.text = model.title
    }
}
