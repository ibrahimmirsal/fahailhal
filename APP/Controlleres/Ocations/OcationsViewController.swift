//
//  OcationsViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class OcationsViewController: UIViewController {

    @IBOutlet weak var tableViewOccation: UITableView!
    @IBOutlet weak var btnDeath: UIButton!
    @IBOutlet weak var btnMarriage: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    let presenter = HomePresenter()
    let indicator = APPLoader.getObject()
    var isdeath = false
    var deathOccationArray = [OccationModel]()
    var marriageOccationArray = [OccationModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func marriage(_ sender: Any) {
        isdeath = !isdeath
        initSelect()
    }
    @IBAction func death(_ sender: Any) {
        isdeath = !isdeath
        initSelect()
    }
    func initSelect () {
        let color = AppUtils.getObject().hexStringToUIColor(hex: "#264B89")
        if !isdeath {
            btnMarriage.backgroundColor = color
            btnMarriage.setTitleColor(.white, for: .normal)
            btnDeath.setTitleColor(color, for: .normal)
            btnDeath.backgroundColor = .white
            if marriageOccationArray.count == 0 {
                getMarriageOccation()
            }
        }else {
            btnDeath.backgroundColor = color
            btnDeath.setTitleColor(.white, for: .normal)
            btnMarriage.setTitleColor(color, for: .normal)
            btnMarriage.backgroundColor = .white
            if deathOccationArray.count == 0 {
               getDeathOccation()
            }
        }
        tableViewOccation.reloadData()
    }
    func initViews () {
        labelTitle.text = TabBarLocalizedString.Occasions
        initTable()
        initSelect()
    }
    func initTable (){
        let nib = UINib(nibName: "OccationTableViewCell", bundle: nil)
        tableViewOccation.register(nib, forCellReuseIdentifier: "OccationTableViewCell")
    }
    func getDeathOccation () {
        indicator.show(self.view, true)
        presenter.getDeathOccations(successBlock: { (model) in
            self.indicator.hide()
            self.deathOccationArray = model.notify
            self.tableViewOccation.reloadData()
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
    func getMarriageOccation () {
        indicator.show(self.view, true)
        presenter.getMarriageOccations(successBlock: { (model) in
            self.indicator.hide()
            self.marriageOccationArray = model.notify
            self.tableViewOccation.reloadData()
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }

}
extension OcationsViewController : UITableViewDelegate , UITableViewDataSource , DiwanMapPopUpProtocol {
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        if isdeath {
            let hight =  screen.height * 0.07
            return hight
            
        }else {
            let hight =  screen.height * 0.07
            return hight
        }
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        if isdeath {
            return deathOccationArray.count
        }else {
            return marriageOccationArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "OccationTableViewCell"
        let cell: OccationTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! OccationTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if isdeath {
            cell.drawOccationCell(model: deathOccationArray[indexPath.row])
        }else {
            cell.drawOccationCell(model: marriageOccationArray[indexPath.row])
        }
        return cell
    }
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = DiwanDetailPOPUPViewController()
        vc.occationModel = isdeath ? deathOccationArray[indexPath.row] : marriageOccationArray[indexPath.row]
        vc.vcTitle = ""
        vc.delegate = self
        AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.4, dismissGesture: true)
    }
    func getInnerDiwan(model: InnerDayModel) {
        let vc = LocationMapViewController()
        vc.diwanModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
