//
//  ViewController.swift
//  APP
//
//  Created by mac on 8/3/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            self.initViews()
        }
      //  initViews()
    }
    
    func initViews () {
        if (UserDefaults.standard.object(forKey: AppConstant.getObject().LANGUAGE_KEY) == nil){
            UserDefaults.standard.set(AppConstant.getObject().ARABIC, forKey: AppConstant.getObject().LANGUAGE_KEY)
        }
        let nav = UINavigationController.init(rootViewController: MainTabBarViewController())
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
        UIView.appearance().semanticContentAttribute = AppLanguageHandler.getObject().IsEnglish() ? .forceLeftToRight : .forceRightToLeft
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }


}

