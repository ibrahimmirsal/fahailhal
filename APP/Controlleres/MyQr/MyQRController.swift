//
//  MyQRController.swift
//  APP
//
//  Created by Ibrahim Sabry on 23/02/2023.
//  Copyright © 2023 ibrahim. All rights reserved.
//

import UIKit

class MyQRController: UIViewController {
    @IBOutlet weak var btnGet: UIButton!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgQR.isHidden = languageHandler.getMyQR().isEmpty
        btnGet.isHidden = !languageHandler.getMyQR().isEmpty
        lblTitle.text = ""
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !languageHandler.getMyQR().isEmpty {
            loadQR()
        }
    }

    @IBAction func getOnTap(_ sender: Any) {
        let vc = InvestorsViewController()
        vc.isQr = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backOnTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func loadQR() {
        imgQR.isHidden = languageHandler.getMyQR().isEmpty
        btnGet.isHidden = !languageHandler.getMyQR().isEmpty
        imgQR.image = generateBarcode(from: languageHandler.getMyQR())
        lblTitle.text = languageHandler.IsEnglish() ? "My QR":"الباركود الخاص بي"
        
    }
    
    func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}
