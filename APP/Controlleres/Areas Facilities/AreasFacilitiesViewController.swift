//
//  AreasFacilitiesViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class AreasFacilitiesViewController: UIViewController {
    @IBOutlet weak var tableViewBranches: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    var branchArray = [BranchModel]()
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initViews () {
        labelTitle.text = TabBarLocalizedString.areafacilities
        initTable()
        getCategoryData()
    }
    func initTable (){
        let nib = UINib(nibName: "InvestBranchTableViewCell", bundle: nil)
        tableViewBranches.register(nib, forCellReuseIdentifier: "InvestBranchTableViewCell")
    }
    func getCategoryData () {
        indicator.show(self.view, true)
        presenter.getBranches(successBlock: { (model) in
            self.indicator.hide()
            for branch in model.branches {
                if (branch.branch_name.contains("الفرع") || branch.branch_name.contains("السوق") || branch.branch_name.contains("الفـــرع")){
                }else {
                    self.branchArray.append(branch)
                }
            }
            self.tableViewBranches.reloadData()
        }) { (error) in
            self.indicator.hide()
        }
    }
}
extension AreasFacilitiesViewController : UITableViewDelegate , UITableViewDataSource , BranchMapProtocol{
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        return screen.height * 0.05
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return branchArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "InvestBranchTableViewCell"
        let cell: InvestBranchTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! InvestBranchTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.drawCellBranch(model: branchArray[indexPath.row])
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = BranchPopUpViewController()
        vc.model = branchArray[indexPath.row]
        vc.delegate = self
        AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.5, dismissGesture: true)
        
    }
    func getOpenBranchLocation(model: BranchModel) {
        let inner = InnerDayModel()
        inner.latitude = model.latitude
        inner.longitude = model.longtude
        let vc = LocationMapViewController()
        vc.diwanModel = inner
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

