//
//  LocationMapViewController.swift
//  APP
//
//  Created by mac on 11/13/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import GoogleMaps
class LocationMapViewController: UIViewController {

    @IBOutlet weak var viewMap: GMSMapView!
    var diwanModel = InnerDayModel()
    var lat  = 0.0
    var lng = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCamera()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initCamera() {
        if diwanModel.latitude.count > 0 {
            lat = Double(diwanModel.latitude)!
            lng = Double(diwanModel.longitude)!
        }
        let camera_loc = CLLocationCoordinate2D.init(latitude: lat, longitude:lng)
        let camera = GMSCameraPosition.camera(withTarget: camera_loc, zoom: 14)
        viewMap.camera = camera
        
        let marker = GMSMarker.init()
        marker.position = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
        marker.icon = UIImage.init(named: "destination")
        marker.map = viewMap
    }
}
