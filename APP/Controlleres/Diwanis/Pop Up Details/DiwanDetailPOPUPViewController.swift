//
//  DiwanDetailPOPUPViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol DiwanMapPopUpProtocol {
    func getInnerDiwan (model:InnerDayModel)
}
class DiwanDetailPOPUPViewController: UIViewController {

    @IBOutlet weak var labelTimeValue: LeftLBL!
    @IBOutlet weak var labelPhoneValue: LeftLBL!
    @IBOutlet weak var labelAddressValue: LeftLBL!
    @IBOutlet weak var labelAreaValue: LeftLBL!
    @IBOutlet weak var labelDayValue: LeftLBL!
    @IBOutlet weak var labelTime: LeftLBL!
    @IBOutlet weak var labelPhone: LeftLBL!
    @IBOutlet weak var labelAddress: LeftLBL!
    @IBOutlet weak var labelArea: LeftLBL!
    @IBOutlet weak var labelDay: LeftLBL!
    var innerModel = InnerDayModel()
    var occationModel  = OccationModel()
    var vcTitle = ""
    var delegate : DiwanMapPopUpProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    func initViews () {
        if innerModel.id != 0 {
            labelDay.text = LoginLocalizedString.day
            if innerModel.day == 1  {
                labelDayValue.text = LoginLocalizedString.saturday
            }else if innerModel.day == 2 {
                labelDayValue.text = LoginLocalizedString.sunday
            }else if innerModel.day == 3 {
                labelDayValue.text = LoginLocalizedString.monday
            }else if innerModel.day == 4 {
                labelDayValue.text = LoginLocalizedString.tuesday
            }else if innerModel.day == 5 {
                labelDayValue.text = LoginLocalizedString.wenthday
            }else if innerModel.day == 6 {
                labelDayValue.text = LoginLocalizedString.saturday
            }else if innerModel.day == 0 {
                labelDayValue.text = LoginLocalizedString.friday
            }
            labelArea.text = LoginLocalizedString.area
            labelAreaValue.text = innerModel.area
            labelAddress.text = LoginLocalizedString.address
            labelAddressValue.text = innerModel.address
            labelPhone.text = LoginLocalizedString.phone
            labelPhoneValue.text = innerModel.phone
            labelTime.text = ""
            labelTimeValue.text = ""

        }else if occationModel.notification_id != 0 {
            labelDay.text = LoginLocalizedString.occation
            labelDayValue.text = occationModel.title
            labelArea.text = LoginLocalizedString.details
            labelAreaValue.text = occationModel.details
            labelAddress.text = LoginLocalizedString.address
            labelAddressValue.text = occationModel.address
            labelPhone.text = LoginLocalizedString.day
            labelPhoneValue.text = occationModel.date
            labelTime.text = LoginLocalizedString.time
            labelTimeValue.text = occationModel.time
            innerModel.latitude = occationModel.latitude
            innerModel.longitude = occationModel.longitude
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func map(_ sender: Any) {
        if innerModel.latitude.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.locationerror)
        }else {
            self.delegate?.getInnerDiwan(model: innerModel)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
