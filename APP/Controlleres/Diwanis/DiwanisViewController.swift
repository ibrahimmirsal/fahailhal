//
//  DiwanisViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class DiwanisViewController: UIViewController , DiwanMapPopUpProtocol {

    @IBOutlet weak var labelTitle: UILabel!
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    var diwanisModel = DiwanisResponseModel()
    var diwanDayArray = [DayShowModel]()
    var karinDiwanArray = [AreaShowModel]()
    var mubarakDiwanArray = [AreaShowModel]()
    var ismubrakselect = false
    @IBOutlet weak var tableViewDiwans: UITableView!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var btnArea: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func area(_ sender: Any) {
        ismubrakselect = false
        initSelect()
    }
    @IBAction func day(_ sender: Any) {
        ismubrakselect = true
        initSelect()
    }

    @IBAction func addDiwanya(_ sender: Any) {
    }
    func initViews () {
        labelTitle.text = TabBarLocalizedString.Diwaniyas
        initTable()
        btnDay.setTitle(LoginLocalizedString.mubarak, for: .normal)
        btnArea.setTitle(LoginLocalizedString.karin, for: .normal)
        initSelect()
        getData()
    }
    func initSelect () {
        let color = AppUtils.getObject().hexStringToUIColor(hex: "#264B89")
        if ismubrakselect {
            btnDay.backgroundColor = color
            btnDay.setTitleColor(.white, for: .normal)
            btnArea.setTitleColor(color, for: .normal)
            btnArea.backgroundColor = .white
        }else {
            btnArea.backgroundColor = color
            btnArea.setTitleColor(.white, for: .normal)
            btnDay.setTitleColor(color, for: .normal)
            btnDay.backgroundColor = .white
        }
        tableViewDiwans.reloadData()
    }
    func initTable (){
        let nib = UINib(nibName: "DiwanTableViewCell", bundle: nil)
        tableViewDiwans.register(nib, forCellReuseIdentifier: "DiwanTableViewCell")
    }
    func getData () {
        indicator.show(self.view, true)
        presenter.getDiwanis(successBlock: { (model) in
            self.diwanisModel = model
            self.indicator.hide()
            self.fillData()
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
    func fillData () {
        for model in diwanisModel.diwaniya {
            if model.day.count > 0 {
                for dayinner in model.day {
                    if dayinner.suterday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.saturday
                        showModel.diwanday = dayinner.suterday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.sunday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.sunday
                        showModel.diwanday = dayinner.sunday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.monday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.monday
                        showModel.diwanday = dayinner.monday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.tuesday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.tuesday
                        showModel.diwanday = dayinner.tuesday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.wenthday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.wenthday
                        showModel.diwanday = dayinner.wenthday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.thursday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.thurthday
                        showModel.diwanday = dayinner.suterday
                        diwanDayArray.append(showModel)
                    }
                    if dayinner.friday.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.friday
                        showModel.diwanday = dayinner.friday
                        diwanDayArray.append(showModel)
                    }
                }
            }
            if model.area.count > 0 {
                for area in model.area {
                    if area.karin.count > 0 {
                        let showModel = DayShowModel()
                        showModel.day = LoginLocalizedString.karin
                        showModel.diwanday = area.karin
                        diwanDayArray.append(showModel)
                        
//                        let showModel = AreaShowModel()
//                        showModel.area = LoginLocalizedString.karin
//                        showModel.diwanArea = area.karin
                       // karinDiwanArray.append(showModel)
                    }
                }
            }
        }
        tableViewDiwans.reloadData()
    }

}

extension DiwanisViewController : UITableViewDelegate , UITableViewDataSource , DiwanCellProtocol {
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
//        if ismubrakselect {
//            let count  =  mubarakDiwanArray[indexPath.row].diwanArea.count
//            let hight =  screen.height * 0.05 +  screen.height * 0.05 * CGFloat(count)
//            return hight
//
//        }else {
            let count  =  diwanDayArray[indexPath.row].diwanday.count
            let hight =  screen.height * 0.05 +  screen.height * 0.06 * CGFloat(count)
            return hight
     //   }
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
     //   if ismubrakselect {
            return diwanDayArray.count
     //   }else {
       //     return karinDiwanArray.count
       // }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "DiwanTableViewCell"
        let cell: DiwanTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DiwanTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
       // if ismubrakselect {
            cell.drawDayCell(dayModel: diwanDayArray[indexPath.row])
         //   cell.drawAreaCell(areaModel: mubarakDiwanArray[indexPath.row])
       // }else {
         //   cell.drawAreaCell(areaModel: karinDiwanArray[indexPath.row])
      //  }
        cell.delegate = self
        return cell
    }
    func getSelectDiwan(model: InnerDayModel, title: String) {
        let vc = DiwanDetailPOPUPViewController()
        vc.innerModel = model
        vc.vcTitle = title
        vc.delegate = self
        AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.4, dismissGesture: true)
    }
    func getInnerDiwan(model: InnerDayModel) {
        let vc = LocationMapViewController()
        vc.diwanModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
    }
    
}
