//
//  DiwanTableViewCell.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol DiwanCellProtocol {
    func getSelectDiwan (model:InnerDayModel , title : String)
}
class DiwanTableViewCell: UITableViewCell {

    @IBOutlet weak var tableViewDiwanDetails: UITableView!
    @IBOutlet weak var labelTitle: LeftLBL!
    var dayShowModel = DayShowModel()
    var areaShowData = AreaShowModel()
    var isdayselect = false
    var delegate : DiwanCellProtocol?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableViewDiwanDetails.delegate = self
        self.tableViewDiwanDetails.dataSource = self
        initTable()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initTable (){
        let nib = UINib(nibName: "InvestBranchTableViewCell", bundle: nil)
        tableViewDiwanDetails.register(nib, forCellReuseIdentifier: "InvestBranchTableViewCell")
    }
    func drawDayCell (dayModel:DayShowModel) {
        labelTitle.text = dayModel.day
        dayShowModel = dayModel
        isdayselect = true
        self.tableViewDiwanDetails.reloadData()
    }
    func drawAreaCell (areaModel : AreaShowModel) {
        labelTitle.text = areaModel.area
        areaShowData = areaModel
        isdayselect = false
        self.tableViewDiwanDetails.reloadData()
    }
   
    
}
extension DiwanTableViewCell : UITableViewDelegate , UITableViewDataSource {
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        return screen.height * 0.05
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        if isdayselect {
            return dayShowModel.diwanday.count
        }else {
            return areaShowData.diwanArea.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "InvestBranchTableViewCell"
        let cell: InvestBranchTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! InvestBranchTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if isdayselect {
            cell.drawDiwanCell(model: dayShowModel.diwanday[indexPath.row])
        }else {
            cell.drawDiwanCell(model: areaShowData.diwanArea[indexPath.row])

        }
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isdayselect {
            self.delegate?.getSelectDiwan(model: dayShowModel.diwanday[indexPath.row], title: dayShowModel.day)
        }else {
            self.delegate?.getSelectDiwan(model: areaShowData.diwanArea[indexPath.row], title: areaShowData.area)
        }
    }
    
}
