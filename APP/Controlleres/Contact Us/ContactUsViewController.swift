//
//  ContactUsViewController.swift
//  APP
//
//  Created by Ibrahim on 10/27/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController , MFMailComposeViewControllerDelegate , UINavigationControllerDelegate {

    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPhoneCall: UIButton!
    @IBOutlet weak var viewMessage: ShadowView!
    @IBOutlet weak var btnWhats: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var textFieldMessage: LeftTextField!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var textFieldTopic: LeftTextField!
    @IBOutlet weak var labelTopic: UILabel!
    @IBOutlet weak var textFieldEmail: LeftTextField!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var textFieldPhone: LeftTextField!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var textFieldName: LeftTextField!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    let presenter = HomePresenter()
    let indicator = APPLoader.getObject()
    let composeVC = MFMailComposeViewController()
    var vcTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func twitter(_ sender: Any) {
        let url = "https://twitter.com/Fahaheelcoop"
        self.openURL(url: url)
    }
    @IBAction func facebook(_ sender: Any) {
        let face = "https://www.facebook.com/Fahaheelcoop"
        self.openURL(url: face)
    }
    @IBAction func instgram(_ sender: Any) {
        let url = "http://instagram.com/Fahaheelcoop"
        self.openURL(url: url)
    }
    @IBAction func flicker(_ sender: Any) {
        if vcTitle == TabBarLocalizedString.callus {
            let url = "https://m.flickr.com/#/photos/127046456@N04/sets/"
            self.openURL(url: url)
        }else {
            let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=+96551789951")!
            if UIApplication.shared.canOpenURL(appURL as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL as URL)
                }
            }
        }
       
    }
    
    @IBAction func whatsApp(_ sender: Any) {
        let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=96551789951")!
        if UIApplication.shared.canOpenURL(appURL as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
    }
    @IBAction func email(_ sender: Any) {
        if let url = URL(string: "mailto:\(textFieldTopic.text!)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

    }
    @IBAction func call(_ sender: Any) {
        if let mobileCallUrl = URL(string: "tel://96551789951") {
            let application = UIApplication.shared
            if application.canOpenURL(mobileCallUrl) {
                if #available(iOS 10.0, *) {
                    application.open(mobileCallUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(mobileCallUrl)
                }
            }
        }
    }
    @IBAction func send(_ sender: Any) {
        self.view.endEditing(true)
        if textFieldName.text!.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.name_required)
        }else if textFieldPhone.text!.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.phone_required)
        }else if textFieldPhone.text!.count < 8 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.invalid_phone)
        }else if textFieldEmail.text!.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.email_required)
        }else if !AppUtils.getObject().isValidEmail(testStr: textFieldEmail.text!) {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.emailValidateMessage)
        }else if textFieldTopic.text!.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.topicrequird)
        }else if textFieldMessage.text!.count == 0 {
            APPSnackbar.getObject().showMessage(message: LoginLocalizedString.messagerequired)
        }else {
            sendEmail()
        }
    }
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            composeVC.mailComposeDelegate = self
            composeVC.delegate  = self
            composeVC.setToRecipients(["Fahaheelcoop@hotmail.com"])
            composeVC.setSubject(textFieldTopic.text!)
            let body = "\(LoginLocalizedString.name):\(textFieldName.text!)\n \(LoginLocalizedString.phone):\(textFieldPhone.text!)\n \(LoginLocalizedString.message):\(textFieldMessage.text!)"
            composeVC.setMessageBody(body, isHTML: false)
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func initViews () {
        labelTitle.text = vcTitle
        if vcTitle == TabBarLocalizedString.callus{
            btnWhats.setImage(UIImage.init(named: "flicker2"), for: .normal)
            labelName.text = LoginLocalizedString.name
            textFieldName.text = TabBarLocalizedString.gamaianame
            textFieldName.isUserInteractionEnabled = false
            labelPhone.text = TabBarLocalizedString.phonecontact
            textFieldPhone.text = "+96551789951"
            textFieldPhone.isUserInteractionEnabled = false
            labelEmail.text = TabBarLocalizedString.whatsappcontact
            textFieldEmail.text = "+96551789951"
            textFieldEmail.isUserInteractionEnabled = false
            labelTopic.text = TabBarLocalizedString.emailcontact
            textFieldTopic.text = "Fahaheelcoop@hotmail.com"
            textFieldTopic.isUserInteractionEnabled = false
            labelMessage.isHidden = true
            viewMessage.isHidden = true
            btnSend.isHidden = true
            labelMessage.text = LoginLocalizedString.message
            btnSend.setTitle(LoginLocalizedString.send, for: .normal)
        }else {
            btnWhats.setImage(UIImage.init(named: "whatsapp"), for: .normal)
            labelName.text = LoginLocalizedString.name
            labelPhone.text = LoginLocalizedString.phone
            labelEmail.text = LoginLocalizedString.email
            labelTopic.text = LoginLocalizedString.topic
            labelMessage.text = LoginLocalizedString.message
            btnSend.setTitle(LoginLocalizedString.send, for: .normal)
            btnWhatsapp.isHidden = true
            btnEmail.isHidden = true
            btnPhoneCall.isHidden = true
        }
        
    }
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
}
