//
//  ActivityTableViewCell.swift
//  APP
//
//  Created by Ibrahim on 11/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class ActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var viewRound: RoundedBoldView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDes: LeftLBL!
    @IBOutlet weak var labelName: LeftLBL!
    @IBOutlet weak var imageViewCategoty: ImageViewFromUrl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func drawCell(model : CategoryModel) {
        labelDes.text = model.content
        labelName.text = model.title
        labelDate.text = model.publish_date.components(separatedBy: .whitespaces).first
        if (model.media.count > 0){
            imageViewCategoty.showImage(url: model.media[0].media_path)
        }else {
            imageViewCategoty.image = UIImage.init(named: "slider1")

        }
        imageViewCategoty.contentMode = .scaleAspectFit
    }
    
    func drawCellBranch(model : BranchModel) {
        labelDes.text = model.address + model.branch_city
        labelName.text = model.branch_name
        viewRound.isHidden = true
        if (model.images.count > 0){
            imageViewCategoty.showImage(url: model.images[0].media_path)
        }else {
            imageViewCategoty.image = UIImage.init(named: "slider1")
            
        }
    }
}
