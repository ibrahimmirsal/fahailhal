//
//  OffersViewController.swift
//  APP
//
//  Created by Ibrahim on 11/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import ImageSlideshow

class OffersViewController: UIViewController {
    
    @IBOutlet weak var tableViewActivities: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    var categoryArray = [CategoryModel]()
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    var comesfromNotification = false
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initViews () {
        labelTitle.text = TabBarLocalizedString.salesoffers
        initTable()
        getCategoryData()
    }
    func initTable (){
        let nib = UINib(nibName: "ActivityTableViewCell", bundle: nil)
        tableViewActivities.register(nib, forCellReuseIdentifier: "ActivityTableViewCell")
    }
    func getCategoryData () {
        indicator.show(self.view, true)
        presenter.getOffers(successBlock: { (model) in
            self.indicator.hide()
            self.categoryArray = model.news
            if self.comesfromNotification {
                if self.categoryArray.count > 0 {
                    self.openSlider(model: self.categoryArray.first!)
                }
            }
            self.tableViewActivities.reloadData()
        }) { (error) in
            self.indicator.hide()
        }
    }
}
extension OffersViewController : UITableViewDelegate , UITableViewDataSource {
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        return screen.height * 0.33
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return categoryArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "ActivityTableViewCell"
        let cell: ActivityTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ActivityTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.drawCell(model: categoryArray[indexPath.row])
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = NewDetailsViewController()
        vc.model = categoryArray[indexPath.row]
      //  self.navigationController?.pushViewController(vc, animated: true)
        let model = categoryArray[indexPath.row]
        openSlider(model: model)
    }
    func openSlider (model:CategoryModel) {
        let slider = ImageSlideshow()
        var InputSourceImage = [InputSource]()
        var sliderimage = [UIImage?]()
        for media in model.media {
            let imagefromurl = ImageViewFromUrl()
            let imgUrl = media.media_path.replacingOccurrences(of: " ", with: "20%")
            imagefromurl.showImage(url: imgUrl)
            sliderimage.append(imagefromurl.image)
            if let king = KingfisherSource(urlString: imgUrl) {
                InputSourceImage.append(king)
            }
        }
        slider.setImageInputs(InputSourceImage)
        slider.slideshowInterval = 5.0
        slider.delegate = self
        slider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slider.contentScaleMode = UIViewContentMode.scaleAspectFill
        let fullScreenController = slider.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
}
extension OffersViewController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("current page:", page)
    }
}
