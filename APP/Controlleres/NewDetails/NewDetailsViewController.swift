//
//  NewDetailsViewController.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import SimpleImageViewer
class NewDetailsViewController: UIViewController {

    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var labelDetails: LeftLBL!
    @IBOutlet weak var labelTitle: LeftLBL!
    @IBOutlet weak var imageViewNew: ImageViewFromUrl!
    var model = CategoryModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        iniViews()
    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func share(_ sender: Any) {
        if model.news_url.count > 0 {
            let text = model.news_url
            let textShare = [text]
            let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }else {
            APPSnackbar.getObject().showMessage(message: TabBarLocalizedString.invalidlink)
        }
      
    }
    func iniViews () {
        initCollection()
        labelTitle.text = model.title
        labelDetails.text = model.content
        if model.media.count > 0 {
      //      imageViewNew.showImage(url: model.media[0].media_path)
        }else {
            imageViewNew.image = UIImage.init(named: "slider1")
        }
        collectionViewImage.reloadData()
    }
    func initCollection () {
        let nib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
        collectionViewImage.register(nib, forCellWithReuseIdentifier: "ImageCollectionViewCell")
    }
}
extension NewDetailsViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionViewImage.frame.width-25)/3
        if indexPath.row <= 3 {
            return CGSize.init(width: width , height : width)
        }else if indexPath.row == 4 {
            return CGSize.init(width: width * 2 , height : width)
        }else {
            return CGSize.init(width: width , height : width)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return model.media.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.drawCell(model: model.media[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = ImageViewFromUrl()
        image.showImage(url: model.media[indexPath.row].media_path)
        let configuration = ImageViewerConfiguration { config in
            config.image = image.image
        }
        let imageViewerController = ImageViewerController(configuration: configuration)
        self.present(imageViewerController, animated: true)
    }
    
}

