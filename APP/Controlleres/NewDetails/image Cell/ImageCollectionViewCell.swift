//
//  ImageCollectionViewCell.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewMedia: ImageViewFromUrl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func drawCell (model:MediaModel) {
        imageViewMedia.showImage(url: model.media_path)
    }
   
}
