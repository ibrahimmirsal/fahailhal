//
//  BranchDetailsViewController.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import SimpleImageViewer
class BranchDetailsViewController: UIViewController {

    @IBOutlet weak var collectionViewImages: UICollectionView!
    @IBOutlet weak var labelBranchSite: LeftLBL!
    @IBOutlet weak var labelBranchFax: LeftLBL!
    @IBOutlet weak var labelBranchEmail: LeftLBL!
    @IBOutlet weak var labelBranchPhone: LeftLBL!
    @IBOutlet weak var labelBranchDays: LeftLBL!
    @IBOutlet weak var labelBranchAddress: LeftLBL!
    @IBOutlet weak var labelBranchName: LeftLBL!
    @IBOutlet weak var imageViewBranch: ImageViewFromUrl!
    var model = BranchModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initViews () {
        initCollection()
        labelBranchName.text = model.branch_name
        labelBranchAddress.text = model.address + model.branch_city
        labelBranchDays.text = model.working_days + model.working_time_shift_1
        if model.phones.count > 0 {
            labelBranchPhone.text = model.phones[0].phone
        }else {
            labelBranchPhone.text = ""
        }
        if model.emails.count > 0 {
            labelBranchEmail.text = model.emails[0].email
        }else {
            labelBranchEmail.text = ""
        }
        if model.websites.count > 0 {
            labelBranchSite.text = model.websites[0].website
        }else {
            labelBranchSite.text = ""
        }
        if model.faxes.count > 0 {
            labelBranchFax.text = model.faxes[0].fax
        }else {
            labelBranchFax.text = ""
        }
        if model.images.count > 0 {
            imageViewBranch.showImage(url: model.images[0].media_path)
        }else {
            imageViewBranch.image = UIImage.init(named: "slider1")
        }
        collectionViewImages.reloadData()
    }
    func initCollection () {
        let nib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
        collectionViewImages.register(nib, forCellWithReuseIdentifier: "ImageCollectionViewCell")
    }
}
extension BranchDetailsViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionViewImages.frame.width-25)/3
        if indexPath.row <= 3 {
            return CGSize.init(width: width , height : width)
        }else if indexPath.row == 4 {
            return CGSize.init(width: width * 2 , height : width)
        }else {
            return CGSize.init(width: width , height : width)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return model.images.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.drawCell(model: model.images[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = ImageViewFromUrl()
        image.showImage(url: model.images[indexPath.row].media_path)
        let configuration = ImageViewerConfiguration { config in
            config.image = image.image
        }
        let imageViewerController = ImageViewerController(configuration: configuration)
        self.present(imageViewerController, animated: true)
    }
    
}

