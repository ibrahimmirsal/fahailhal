//
//  BranchPopUpViewController.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol BranchMapProtocol {
    func getOpenBranchLocation(model:BranchModel)
}
class BranchPopUpViewController: UIViewController {

    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var labelMap: UILabel!
    @IBOutlet weak var labelWebsite: LeftLBL!
    @IBOutlet weak var labelFaxValue: LeftLBL!
    @IBOutlet weak var labelEmailValue: LeftLBL!
    @IBOutlet weak var labelPhoneValue: LeftLBL!
    @IBOutlet weak var labelWorkingHourValue: LeftLBL!
    @IBOutlet weak var labelAddressValue: LeftLBL!
    @IBOutlet weak var labelNameValue: LeftLBL!
    var delegate : BranchMapProtocol?
    var model = BranchModel()
    var comesFromMap = false
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    func initViews () {
        labelNameValue.text = model.branch_name
        labelAddressValue.text = model.address + model.branch_city
        labelWorkingHourValue.text = model.working_days + model.working_time_shift_1
        if model.phones.count > 0 {
            labelPhoneValue.text = model.phones[0].phone
        }else {
            labelPhoneValue.text = ""
        }
        if model.emails.count > 0 {
            labelEmailValue.text = model.emails[0].email
        }else {
            labelEmailValue.text = ""
        }
        if model.websites.count > 0 {
            labelWebsite.text = model.websites[0].website
        }else {
            labelWebsite.text = ""
        }
        if model.faxes.count > 0 {
            labelFaxValue.text = model.faxes[0].fax
        }else {
            labelFaxValue.text = ""
        }
        if comesFromMap {
            labelMap.isHidden = true
            btnMap.isHidden = true
        }
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gotoMap(_ sender: Any) {
        self.delegate?.getOpenBranchLocation(model: model)
        self.dismiss(animated: true, completion: nil)
    }
}
