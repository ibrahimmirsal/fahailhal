//
//  ProductsResponseModel.swift
//  APP
//
//  Created by mac on 11/14/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class ProductsResponseModel : Mappable {
    var products  = [ProductModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        products <- map["products"]
    }
}
