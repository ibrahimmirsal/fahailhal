//
//  CategoryResponseModel.swift
//  APP
//
//  Created by Ibrahim on 11/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class CategoryResponseModel : Mappable {
   var news  = [CategoryModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        news <- map["news"]
    }
}
