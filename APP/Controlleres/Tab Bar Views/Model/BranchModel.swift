//
//  BranchModel.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class BranchModel: Mappable {
    var branch_id  = 0
    var branch_name = ""
    var description = ""
    var branch_city = ""
    var address = ""
    var working_days = ""
    var working_time_shift_1 = ""
    var working_time_shift_2 = ""
    var language_name = ""
    var lang_abbr = ""
    var facebook = ""
    var twitter = ""
    var linkedin = ""
    var youtube = ""
    var googleplus = ""
    var latitude = ""
    var longtude = ""
    var branch_category = ""
    var country_name = ""
    var phones = [PhoneModel]()
    var emails = [EmailModel]()
    var websites = [WebsSiteModel]()
    var faxes = [FaxModel]()
    var images = [MediaModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        branch_id <- map["branch_id"]
        branch_name <- map["branch_name"]
        description <- map["description"]
        branch_city <- map["branch_city"]
        address <- map["address"]
        working_days <- map["working_days"]
        working_time_shift_1 <- map["working_time_shift_1"]
        working_time_shift_2 <- map["working_time_shift_2"]
        
        language_name <- map["language_name"]
        lang_abbr <- map["lang_abbr"]
        facebook <- map["facebook"]
        twitter <- map["twitter"]
        linkedin <- map["linkedin"]
        youtube <- map["youtube"]
        googleplus <- map["googleplus"]
        latitude <- map["latitude"]
        
        longtude <- map["longtude"]
        branch_category <- map["branch_category"]
        country_name <- map["country_name"]
        phones <- map["phones"]
        emails <- map["emails"]
        websites <- map["websites"]
        faxes <- map["faxes"]
        images <- map["images"]

    }
}


