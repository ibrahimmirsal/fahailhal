//
//  WebsSiteMode.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class WebsSiteModel: Mappable {
    var website  = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        website <- map["website"]
    }
}
