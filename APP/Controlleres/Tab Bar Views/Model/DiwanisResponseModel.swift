//
//  DiwanisResponseModel.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class DiwanisResponseModel : Mappable {
    var diwaniya  = [DiwanModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        diwaniya <- map["diwaniya"]
    }
}
