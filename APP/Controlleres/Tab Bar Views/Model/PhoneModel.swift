//
//  PhoneModel.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class PhoneModel: Mappable {
    var phone  = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        phone <- map["phone"]
    }
}
