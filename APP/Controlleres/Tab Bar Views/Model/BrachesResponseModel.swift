//
//  BrachesResponseModel.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class BrachesResponseModel : Mappable {
    var branches  = [BranchModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        branches <- map["branches"]
    }
}
