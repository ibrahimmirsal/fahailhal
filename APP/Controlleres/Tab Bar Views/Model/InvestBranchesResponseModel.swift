//
//  InvestBranchesResponseModel.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class InvestBranchesResponseModel : Mappable {
    var branches  = [InvestBranchModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        branches <- map["stores"]
    }
}
