//
//  InvestBranchModel.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class InvestBranchModel: Mappable {
    var store_id  = 0
    var store_name = ""
    var store_address = ""
    var store_phone = ""
    var store_open_time = ""
    var store_close_time = ""
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        store_id <- map["store_id"]
        store_name <- map["store_name"]
        store_address <- map["store_address"]
        store_phone <- map["store_phone"]
        store_open_time <- map["store_open_time"]
        store_close_time <- map["store_close_time"]

    }
}


