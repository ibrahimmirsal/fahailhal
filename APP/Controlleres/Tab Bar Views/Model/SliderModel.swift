//
//  SliderModel.swift
//  APP
//
//  Created by mac on 11/25/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class SliderModel: Mappable {
    var image_path  = ""
    var title = ""
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        image_path <- map["image_path"]
        title <- map["title"]
    }
}
