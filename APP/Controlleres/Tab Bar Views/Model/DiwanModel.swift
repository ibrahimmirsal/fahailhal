//
//  DiwanModel.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class DiwanModel: Mappable {
    var day  = [DayModel]()
    var area = [AreaModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        day <- map["day"]
        area <- map["area"]
    }
}
