//
//  DayModel.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class DayModel: Mappable {
    var suterday  = [InnerDayModel]()
    var sunday  = [InnerDayModel]()
    var monday  = [InnerDayModel]()
    var tuesday  = [InnerDayModel]()
    var wenthday  = [InnerDayModel]()
    var thursday  = [InnerDayModel]()
    var friday  = [InnerDayModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        suterday <- map["0 "]
        sunday <- map["1 "]
        monday <- map["2 "]
        tuesday <- map["3 "]
        wenthday <- map["4 "]
        thursday <- map["5 "]
        friday <- map["6 "]
    }
}
class DayShowModel {
    var day = ""
    var diwanday = [InnerDayModel]()
}
