//
//  InvestProfiteResponseModel.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class InvestProfiteResponseModel : Mappable {
    var profit  = [InvestorProfitModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        profit <- map["profit"]
    }
}
