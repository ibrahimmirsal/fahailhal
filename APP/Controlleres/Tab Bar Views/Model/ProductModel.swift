//
//  ProductModel.swift
//  APP
//
//  Created by mac on 11/14/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class ProductModel: Mappable {
    var product_id  = 0
    var product_name = ""
    var product_features = ""
    var product_description = ""
    var product_category_name = ""
    var product_barcode = ""
    var product_original_barcode = ""
    var product_list_price = 0.0
    var product_price = 0.0
    var product_cost = 0.0
    var product_discount = 0.0
    var product_expiry_date = ""
    var product_currency = ""
    var images = [MediaModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        product_id <- map["product_id"]
        product_name <- map["product_name"]
        product_features <- map["product_features"]
        product_description <- map["product_description"]
        product_category_name <- map["product_category_name"]
        product_barcode <- map["product_barcode"]
        product_original_barcode <- map["product_original_barcode"]
        product_list_price <- map["product_list_price"]
        product_cost <- map["product_cost"]
        product_discount <- map["product_discount"]
        product_expiry_date <- map["product_expiry_date"]
        product_currency <- map["product_currency"]
        images <- map["images"]
    }
}

