//
//  EmailModel.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class EmailModel: Mappable {
    var email  = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        email <- map["email"]
    }
}
