//
//  OccationResponseModel.swift
//  APP
//
//  Created by mac on 11/13/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class OccationResponseModel : Mappable {
    var notify  = [OccationModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        notify <- map["notify"]
    }
}
