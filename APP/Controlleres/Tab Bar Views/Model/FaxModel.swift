//
//  FaxModel.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class FaxModel: Mappable {
    var fax  = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        fax <- map["fax"]
    }
}
