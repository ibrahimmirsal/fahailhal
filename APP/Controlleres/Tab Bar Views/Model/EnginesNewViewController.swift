//
//  EnginesNewViewController.swift
//  APP
//
//  Created by Ibrahim on 10/30/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class EnginesNewViewController: UIViewController {
      @IBOutlet weak var btnRetry: RetryBtn!
        @IBOutlet weak var labelError: CenterLBL!
        @IBOutlet weak var SearchBtn: UIButton!
        @IBOutlet weak var TitleLabel: TitleLBL!
        @IBOutlet weak var collectionview: UICollectionView!
        var auctionsArray = [HomeModel]()
        let indicator = APPLoader.getObject()
        let presenter = HomePresenter()
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
            initViews()
        }

        @IBAction func SearchBtn(_ sender: Any) {
            self.navigationController?.pushViewController(SearchViewController(), animated: true)
        }
        @IBAction func retry(_ sender: Any) {
            initViews()
        }
        
        func initViews () {
            self.labelError.isHidden = true
            self.btnRetry.isHidden = true
            self.TitleLabel.text = TabLocalizedString.engine
            initCollection()
            getAuction()
        }
        func initCollection () {
            let nib = UINib(nibName: "MazadCell", bundle: nil)
            collectionview.register(nib, forCellWithReuseIdentifier: "MazadCell")
            collectionview.dataSource = self
            collectionview.delegate = self
        }
        func getAuction () {
            if auctionsArray.count == 0 {
                indicator.show(self.view, true)
            }
            presenter.getEngines(successBlock: { (auctions) in
                self.indicator.hide()
                self.auctionsArray = auctions.data
                if self.auctionsArray.count == 0 {
                    self.labelError.text = HomeLocalizedString.nodata
                    self.labelError.isHidden = false
                }else {
                    self.labelError.isHidden = true
                    self.btnRetry.isHidden = true
                }
                self.collectionview.reloadData()
            }) { (error) in
                self.indicator.hide()
                self.labelError.isHidden = false
                self.btnRetry.isHidden = false
                self.btnRetry.setTitle(AlertLocalizedString.retry, for: .normal)
                self.labelError.text = AlertLocalizedString.expected_error
                
            }
        }


    }
extension EnginesNewViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return auctionsArray.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MazadCell", for: indexPath) as! MazadCell
            cell.drawCell(model: auctionsArray[indexPath.row])
            return cell
            
            
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                let width = (collectionView.frame.width-30)/2
            return CGSize.init(width: width , height:UIScreen.main.bounds.size.width * 0.5)
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let vc = ItemDetailsViewController()
            vc.id = auctionsArray[indexPath.row].id
            vc.vcTitle = auctionsArray[indexPath.row].name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            UIView.animate(withDuration: 1, animations: {
                self.viewSlideInFromBottom(toTop: cell)
            })
        }
}
