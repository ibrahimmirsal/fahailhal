//
//  MediaModel.swift
//  APP
//
//  Created by Ibrahim on 11/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper

class MediaModel: Mappable {
    var media_path  = ""
    var media_type_name = ""
  
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        media_path <- map["media_path"]
        media_type_name <- map["media_type_name"]
    }
}
