//
//  SliderResponseModel.swift
//  APP
//
//  Created by mac on 11/25/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class SliderResponseModel : Mappable {
    var ads  = [SliderModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        ads <- map["ads"]
    }
}
