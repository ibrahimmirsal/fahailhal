//
//  InnerDayModel.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class InnerDayModel: Mappable {
    var id  = 0
    var name = ""
    var area = ""
    var day = 0
    var dayfalse = false
    var address = ""
    var latitude = ""
    var longitude = ""
    var details = ""
    var phone = ""
    var media = [MediaModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        area <- map["area"]
        day <- map["day"]
        dayfalse <- map["day"]
        address <- map["address"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        details <- map["details"]
        phone <- map["phone"]
        media <- map["media"]
    }
}


