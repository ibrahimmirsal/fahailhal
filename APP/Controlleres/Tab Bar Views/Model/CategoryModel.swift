//
//  CategoryModel.swift
//  APP
//
//  Created by Ibrahim on 11/7/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper

class CategoryModel: Mappable {
    var news_id  = 0
    var create_date = ""
    var latitude = ""
    var longitude = ""
    var publish_date = ""
    var news_url = ""
    var category_name = ""
    var status_name = ""
    var content = ""
    var title = ""
    var subtitle = ""
    var author = ""
    var media = [MediaModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        news_id <- map["news_id"]
        create_date <- map["create_date"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        publish_date <- map["publish_date"]
        news_url <- map["news_url"]
        category_name <- map["category_name"]
        status_name <- map["status_name"]
        content <- map["content"]
        title <- map["title"]
        subtitle <- map["subtitle"]
        author <- map["author"]
        media <- map["media"]
    }
}

