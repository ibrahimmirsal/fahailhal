//
//  InvestorProfitModel.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class InvestorProfitModel: Mappable {
    
    var share_holder_id  = 0
    var share_holder_name = ""
    var share_profit : Float = 0
    var civil_id_number = ""
    var box_number = ""
    var year = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        share_holder_id <- map["share_holder_id"]
        share_holder_name <- map["share_holder_name"]
        share_profit <- map["share_profit"]
        civil_id_number <- map["civil_id_number"]
        box_number <- map["box_number"]
        year <- map["year"]
    }
}


