//
//  OccationModel.swift
//  APP
//
//  Created by mac on 11/13/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class OccationModel: Mappable {
    var notification_id  = 0
    var title = ""
    var date = ""
    var time = ""
    var details = ""
    var latitude = ""
    var longitude = ""
    var address = ""
    var cat_name = ""
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        notification_id <- map["notification_id"]
        title <- map["title"]
        date <- map["date"]
        time <- map["time"]
        details <- map["details"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        address <- map["address"]
        cat_name <- map["cat_name"]
    }
}

