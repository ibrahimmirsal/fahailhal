//
//  AreaModel.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper
class AreaModel: Mappable {
    var karin  = [InnerDayModel]()
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map)
    {
        karin <- map["الفحيحيل"]
    }
}
class AreaShowModel {
    var area = ""
    var diwanArea = [InnerDayModel]()
}
