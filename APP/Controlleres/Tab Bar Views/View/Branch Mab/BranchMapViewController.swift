//
//  BranchMapViewController.swift
//  APP
//
//  Created by mac on 11/20/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import GoogleMaps

class BranchMapViewController: UIViewController , GMSMapViewDelegate , BranchMapProtocol {
    @IBOutlet weak var mapView: GMSMapView!
    var branchArray = [BranchModel]()
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    func initViews () {
        initCamera()
        mapView.delegate = self
        getCategoryData()
    }
    func initCamera () {
        let camera_loc = CLLocationCoordinate2D.init(latitude: 29.0861152, longitude: 48.12127320000002)
        let camera = GMSCameraPosition.camera(withTarget: camera_loc, zoom: 13)
        mapView.camera = camera
    }
    func getCategoryData () {
        indicator.show(self.view, true)
        presenter.getBranches(successBlock: { (model) in
            self.indicator.hide()
            self.branchArray = model.branches
            self.drawMarkers()
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
    func drawMarkers () {
        self.mapView.clear()
        for branch in branchArray {
            if branch.latitude.count > 0 {
                let marker = GMSMarker.init()
                let lat = Double(branch.latitude)!
                let lng = Double(branch.longtude)!
                marker.position = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                marker.icon = UIImage.init(named: "destination")
                marker.map = mapView
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
       
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        for branch in branchArray {
            if branch.latitude.count > 0 {
                let lat = Double(branch.latitude)!
                let lng = Double(branch.longtude)!
                if (lat == marker.position.latitude && lng == marker.position.longitude) {
                    let vc = BranchPopUpViewController()
                    vc.model = branch
                    vc.comesFromMap = true
                    vc.delegate = self
                    AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.5, dismissGesture: true)
                }
            }
        }
        return true
    }
    func getOpenBranchLocation(model: BranchModel) {
        let inner = InnerDayModel()
        inner.latitude = model.latitude
        inner.longitude = model.longtude
        let vc = LocationMapViewController()
        vc.diwanModel = inner
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

