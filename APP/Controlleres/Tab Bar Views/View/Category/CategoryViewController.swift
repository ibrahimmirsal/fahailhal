//
//  CategoryViewController.swift
//  APP
//
//  Created by Ibrahim on 11/6/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {

    @IBOutlet weak var collectionViewCategory: UICollectionView!
    var homeArray = [HomeModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
     func initViews() {
            self.view.backgroundColor = .white
            initCollection()
            fillHomeArray()
        }
        func initCollection () {
            let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
            collectionViewCategory.register(nib, forCellWithReuseIdentifier: "HomeCollectionViewCell")
            
            let nib2 = UINib(nibName: "CallUsCollectionViewCell", bundle: nil)
            collectionViewCategory.register(nib2, forCellWithReuseIdentifier: "CallUsCollectionViewCell")
            
        }
        func fillHomeArray() {
            homeArray = []
            let complain = HomeModel()
            complain.name = TabBarLocalizedString.complain
            complain.image = "complaint"
            homeArray.append(complain)
    
            let Occasions = HomeModel()
            Occasions.name = TabBarLocalizedString.Occasions
            Occasions.image = "calendar"
            homeArray.append(Occasions)
            
            let dyo = HomeModel()
            dyo.name = TabBarLocalizedString.Diwaniyas
            dyo.image = "directory-1"
            homeArray.append(dyo)
            
            let record = HomeModel()
            record.name = TabBarLocalizedString.diwaniyarecord
            record.image = "form"
            homeArray.append(record)
            
            let list = HomeModel()
            list.name = TabBarLocalizedString.shoppinglist
            list.image = "checklist"
            homeArray.append(list)
            
            let revetse = HomeModel()
            revetse.name = TabBarLocalizedString.reserve
            revetse.image = "social-care"
            homeArray.append(revetse)
            
            let qr = HomeModel()
            qr.name = TabBarLocalizedString.barcode
            qr.image = "firework"
            homeArray.append(qr)
            
            let myqr = HomeModel()
            myqr.name = TabBarLocalizedString.mycivil
            myqr.image = "firework"
         //   homeArray.append(myqr)
//            let callus = HomeModel()
//            callus.name = TabBarLocalizedString.callus
//            callus.image = "phone-book"
//            homeArray.append(callus)
            collectionViewCategory.reloadData()

        }
}
extension CategoryViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if homeArray[indexPath.row].name == TabBarLocalizedString.callus || homeArray[indexPath.row].name == TabBarLocalizedString.shoppinglist {
//            let width = collectionViewCategory.frame.width
//            return CGSize.init(width: width , height : collectionViewCategory.frame.width * 0.5)
//        }else {
            if AppUtils.getObject().isDeviceiPad() {
                let width = (collectionViewCategory.frame.width-10) * 0.3
                return CGSize.init(width: width , height : collectionViewCategory.frame.width * 0.35)
            }else {
                let width = (collectionViewCategory.frame.width-25)/2
                return CGSize.init(width: width , height : collectionViewCategory.frame.width * 0.5)
            }

       
   //     }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return homeArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

//        if homeArray[indexPath.row].name == TabBarLocalizedString.callus || homeArray[indexPath.row].name == TabBarLocalizedString.shoppinglist {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CallUsCollectionViewCell", for: indexPath) as! CallUsCollectionViewCell
//            cell.layer.masksToBounds = false
//            cell.drawCell(model: homeArray[indexPath.row])
//            return cell
//        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            cell.layer.masksToBounds = false
            cell.drawCell(model: homeArray[indexPath.row])
            return cell

    //    }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = homeArray[indexPath.row]
        if model.name == TabBarLocalizedString.service {
            let vc = InvestBranchesViewController()
            vc.vcTitle = TabBarLocalizedString.service
            self.navigationController?.pushViewController(vc, animated: true)
         //   self.navigationController?.pushViewController(MaintainanceViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.complain {
            let vc = ContactUsViewController()
            vc.vcTitle = TabBarLocalizedString.complain
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name == TabBarLocalizedString.Occasions {
            self.navigationController?.pushViewController(OcationsViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.Diwaniyas {
            self.navigationController?.pushViewController(DiwanisViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.shoppinglist {
            self.navigationController?.pushViewController(PriceListViewController(), animated: true)
        }else if model.name ==  TabBarLocalizedString.callus {
            let vc = AboutUsViewController()
            vc.vcTitle = TabBarLocalizedString.callus
            self.navigationController?.pushViewController(vc, animated: true)
//            let vc = ContactUsViewController()
//            vc.vcTitle = TabBarLocalizedString.callus
//            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name == TabBarLocalizedString.diwaniyarecord {
            let appURL = NSURL(string: "https://api.whatsapp.com/send?phone=+96551789951")!
            if UIApplication.shared.canOpenURL(appURL as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL as URL)
                }
            }
        }else if model.name == TabBarLocalizedString.reserve  {
            let vc = WebViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name == TabBarLocalizedString.barcode  {
            let vc = BarCodeViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name == TabBarLocalizedString.mycivil  {
            let vc = MyQRController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 1, animations: {
            if AppLanguageHandler.getObject().IsEnglish() {
                if indexPath.row % 2 == 0 {
                    self.viewSlideInFromLeft(toRight: cell)
                }
                else {
                    self.viewSlideInFromRight(toLeft: cell)
                }
            }else {
                if indexPath.row % 2 == 0 {
                    self.viewSlideInFromRight(toLeft: cell)
                }
                else {
                    self.viewSlideInFromLeft(toRight: cell)
                }
            }
        })
    }

}
