//
//  HomeViewController.swift
//  APP
//
//  Created by mac on 8/4/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import Firebase
import SimpleImageViewer
class HomeViewController: UIViewController , UIScrollViewDelegate , PHCyclePictureViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollViewSlider: UIScrollView!
    @IBOutlet weak var constraintCollectionHIGHT: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHome: UICollectionView!
    @IBOutlet weak var viewSlider: UIView!
    @IBOutlet weak var imageViewlogo: UIImageView!
    var homeArray = [HomeModel]()
    var sliderArray = [Slider]()
    var sliderCustomArray = [String]()
    var sliderImageArray = [UIImage]()
    let presenter = HomePresenter()
    let indicator = APPLoader.getObject()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        indicator.show(self.view, true)
        getSlider()
        if UserDefaults.standard.object(forKey: AppConstant.getObject().DEVICE_TOKEN) != nil {
            self.getRegisterToken()
        }        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initObservers()
        if UserDefaults.standard.object(forKey: AppConstant.getObject().DEVICE_TOKEN) == nil {
            getToken()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    func initObservers () {
        NotificationCenter.default.addObserver(self,selector: #selector(self.openOffers), name:NSNotification.Name(rawValue: "openOffers"),object: nil)
         NotificationCenter.default.addObserver(self,selector: #selector(self.openNews), name:NSNotification.Name(rawValue: "openNews"),object: nil)
         NotificationCenter.default.addObserver(self,selector: #selector(self.openActivities), name:NSNotification.Name(rawValue: "openActivities"),object: nil)
         NotificationCenter.default.addObserver(self,selector: #selector(self.openUrl), name:NSNotification.Name(rawValue: "openUrl"),object: nil)
        if AppNotificationHandler.getObject().comesFromNotifivication || AppNotificationHandler.getObject().userInfo.count > 0 {
            AppNotificationHandler.getObject().comesFromNotifivication = true
            AppNotificationHandler.getObject().parseRemoteNotification(userInfoData: AppNotificationHandler.getObject().userInfo)
        }
    }
    @objc func openOffers() {
        let vc = OffersViewController()
        vc.comesfromNotification = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func openNews() {
        let vc = NewsViewController()
        vc.comesfromNotification = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func openActivities() {
        let vc = ActivitiesViewController()
        vc.comesfromNotification = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func openUrl() {
        let url = UserDefaults.standard.string(forKey: "notifurl") ?? ""
        gotoURL(url: url)
    }
    func gotoURL (url:String){
        if let url = URL(string: "\(url)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    func getToken () {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.set(result.token, forKey: AppConstant.getObject().DEVICE_TOKEN)
                self.getRegisterToken()
            }
        }
    }
    func getRegisterToken () {
        self.presenter.getRegisterDevice(successBlock: { (model) in
            
        }, failureBlock: { (error) in
            
        })

    }
    func initViews() {
        self.view.backgroundColor = .white
        initCollection()
        fillHomeArray()
    }
    func getSlider () {
        presenter.getSlider(successBlock: { (model) in
            self.indicator.hide()
            for ads in model.ads {
                self.sliderCustomArray.append(ads.image_path)
            }
            self.createCustomSlider()
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
    func initCollection () {
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionViewHome.register(nib, forCellWithReuseIdentifier: "HomeCollectionViewCell")
    }
    func fillHomeArray() {
        homeArray = []
        let offers = HomeModel()
        offers.name = TabBarLocalizedString.salesoffers
        offers.image = "firework"
        homeArray.append(offers)
        
        let activities = HomeModel()
        activities.name = TabBarLocalizedString.socialactivities
        activities.image = "social-care"
        homeArray.append(activities)
       
        let news = HomeModel()
        news.name = TabBarLocalizedString.news
        news.image = "newspaper"
        homeArray.append(news)
        
        let about = HomeModel()
        about.name = TabBarLocalizedString.aboutus
        about.image = "exclamation-mark"
        homeArray.append(about)
        
        let branch = HomeModel()
        branch.name = TabBarLocalizedString.branchesdirectory
        branch.image = "directory-1"
        homeArray.append(branch)
        
        let investbranches = HomeModel()
        investbranches.name = TabBarLocalizedString.investbranches
        investbranches.image = "earnings"
        homeArray.append(investbranches)
        
        let Shareholdersprofits = HomeModel()
        Shareholdersprofits.name = TabBarLocalizedString.Shareholdersprofits
        Shareholdersprofits.image = "coins"
        homeArray.append(Shareholdersprofits)
        
        let areafacilities = HomeModel()
        areafacilities.name = TabBarLocalizedString.areafacilities
        areafacilities.image = "medical-center"
        homeArray.append(areafacilities)
        
        collectionViewHome.reloadData()
        constraintCollectionHIGHT.constant = collectionViewHome.frame.width  * 0.5 * 0.5 * CGFloat(homeArray.count) + 40
        collectionViewHome.updateConstraints()
        sliderImageArray = []
        sliderImageArray.append(UIImage.init(named: "slider")!)
        sliderImageArray.append(UIImage.init(named: "slider2")!)
        sliderImageArray.append(UIImage.init(named: "slider3")!)
        sliderImageArray.append(UIImage.init(named: "slider4")!)
        sliderImageArray.append(UIImage.init(named: "slider5")!)
        sliderImageArray.append(UIImage.init(named: "slider6")!)
        sliderImageArray.append(UIImage.init(named: "slider7")!)
       // pageControl.numberOfPages = sliderImageArray.count
        //createSlides()
        pageControl.isHidden = true
    }
    func createCustomSlider () {
        let viewCycle = PHCyclePictureView()
        viewCycle.imagePaths = sliderCustomArray
        viewCycle.delegate = self
      //  viewCycle.setGradientLeftToRight(ColorLeft: #colorLiteral(red: 0.9228883386, green: 0.3642458916, blue: 0.3631167412, alpha: 1), ColorRight: #colorLiteral(red: 0.9851198792, green: 0.4905954003, blue: 0.4105719924, alpha: 1))
        viewCycle.anchorBackgroundColor = UIColor.clear
        viewCycle.layer.cornerRadius = 8
        viewCycle.currentDotColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().GRAY_COLOR)
        viewCycle.pageControlPosition = .right
        viewCycle.isAutoScroll = true
        viewCycle.placeholderImage = UIImage.init(named: "LOGO")
        viewCycle.frame = scrollViewSlider.bounds
        scrollViewSlider.addSubview(viewCycle)
    }
    func cyclePictureView(_ cyclePictureView: PHCyclePictureView, didTapItemAt index: Int) {
        //  AppUtils.getObject().openURL(url: sliderArray[index].link)
        if sliderCustomArray.count > 0 {
            let image = ImageViewFromUrl()
            image.showImage(url: sliderCustomArray[index])
            let configuration = ImageViewerConfiguration { config in
                config.image = image.image
            }
            let imageViewerController = ImageViewerController(configuration: configuration)
            self.present(imageViewerController, animated: true)
        }
    }
    func createSlides() {
        for item in sliderImageArray {
               let slide: Slider = Bundle.main.loadNibNamed("Slider", owner: self, options: nil)?.first as! Slider
               slide.imageViewSlider.image = item
            slide.imageViewSlider.contentMode = .scaleToFill
              sliderArray.append(slide)
           }
       }
    override func viewDidLayoutSubviews() {
         super.viewDidLayoutSubviews()
         setupSlideScrollView(slides: sliderArray)
     }
    func setupSlideScrollView(slides : [Slider]) {
           scrollViewSlider.delegate = self
           scrollViewSlider.showsVerticalScrollIndicator = false
           scrollViewSlider.showsHorizontalScrollIndicator = false
           scrollViewSlider.clipsToBounds = true
           scrollViewSlider.isPagingEnabled = true
           if !AppLanguageHandler.getObject().IsEnglish() {
               self.scrollViewSlider.transform = CGAffineTransform(scaleX:-1,y: 1)
               for subView in scrollViewSlider.subviews {
                   subView.transform = CGAffineTransform(scaleX:-1,y: 1)
               }
           }
           scrollViewSlider.contentSize = CGSize(width: UIScreen.main.bounds.width * CGFloat(sliderArray.count), height: scrollViewSlider.frame.height)

           for (index, slider) in sliderArray.enumerated() {
               slider.frame = CGRect(x: UIScreen.main.bounds.width * CGFloat(index), y: 0, width: UIScreen.main.bounds.width - 10 , height: scrollViewSlider.frame.height)
               scrollViewSlider.addSubview(slider)
           }
       }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = Int(scrollView.contentOffset.x/scrollView.frame.width)
        pageControl.currentPage = Int(pageIndex)
    
    }
}
extension HomeViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if AppUtils.getObject().isDeviceiPad() {
            let width = (collectionViewHome.frame.width-10) * 0.22
            return CGSize.init(width: width , height : collectionViewHome.frame.width * 0.22)
        }else {
            let width = (collectionViewHome.frame.width-10)/2
            return CGSize.init(width: width , height : collectionViewHome.frame.width * 0.5)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return homeArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.layer.masksToBounds = false
        cell.drawCell(model: homeArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = homeArray[indexPath.row]
        if model.name ==  TabBarLocalizedString.salesoffers {
            self.navigationController?.pushViewController(OffersViewController(), animated: true)
        }else if model.name ==  TabBarLocalizedString.socialactivities {
            self.navigationController?.pushViewController(ActivitiesViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.news {
            self.navigationController?.pushViewController(NewsViewController(), animated: true)
        }else if model.name ==  TabBarLocalizedString.aboutus {
            let vc = AboutUsViewController()
            vc.vcTitle = TabBarLocalizedString.aboutus
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name == TabBarLocalizedString.branchesdirectory {
            self.navigationController?.pushViewController(BranchesViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.investbranches {
            let vc = InvestBranchesViewController()
            vc.vcTitle = TabBarLocalizedString.Investors
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.name ==  TabBarLocalizedString.Shareholdersprofits {
            self.navigationController?.pushViewController(InvestorsViewController(), animated: true)
        }else if model.name == TabBarLocalizedString.areafacilities {
            self.navigationController?.pushViewController(AreasFacilitiesViewController(), animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 1, animations: {
            if AppLanguageHandler.getObject().IsEnglish() {
                if indexPath.row % 2 == 0 {
                    self.viewSlideInFromLeft(toRight: cell)
                }
                else {
                    self.viewSlideInFromRight(toLeft: cell)
                }
            }else {
                if indexPath.row % 2 == 0 {
                    self.viewSlideInFromRight(toLeft: cell)
                }
                else {
                    self.viewSlideInFromLeft(toRight: cell)
                }
            }
        })
    }

}
