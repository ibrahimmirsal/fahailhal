//
//  HomeCollectionViewCell.swift
//  APP
//
//  Created by Ibrahim on 10/28/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelCategoryName: UILabel!
    @IBOutlet weak var imageViewCategory: ImageViewFromUrl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func drawCell (model : HomeModel) {
        labelCategoryName.text = model.name
        imageViewCategory.image = UIImage.init(named: model.image)
        AppUtils.getObject().makeShadow(view: self.contentView)
    }
}
