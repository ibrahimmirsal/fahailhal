//
//  BarCodeViewController.swift
//  APP
//
//  Created by Ibrahim on 10/27/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import BarcodeScanner

class BarCodeViewController: UIViewController {

    @IBOutlet weak var imageViewGif: UIImageView!
    @IBOutlet weak var btnBarCode: UIButton!
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    func initViews () {
        if let path = Bundle.main.url(forResource: "hover-scan-animated", withExtension: "gif") {
            if let imagedata = NSData(contentsOf: path) {
            //    let imagegif = UIImage.init(data: imagedata as Data)
                imageViewGif.image = UIImage.gif(data: imagedata as Data)
            }
        }
        btnBarCode.setTitle("قارئ البارالكود", for: .normal)
    }
    @IBAction func barcode(_ sender: Any) {
        let vc = BarcodeScannerViewController()
        vc.codeDelegate = self
        vc.errorDelegate = self
        vc.dismissalDelegate = self
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backOnTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension BarCodeViewController: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
        getProducts(code: code)
        controller.dismiss(animated: true, completion: nil)
    }
    func getProducts (code:String) {
        indicator.show(self.view, true)
        presenter.getProducts(code: code, successBlock: { (model) in
            self.indicator.hide()
            if model.products.count > 0 {
                let product  = model.products[0]
                let vc = InvestBranchDetailsPopUpViewController()
                vc.product = product
                AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.3, dismissGesture: true)
            }else {
                self.showError(LoginLocalizedString.nodata, "")
            }
        }) { (error) in
            self.indicator.hide()
            APPSnackbar.getObject().showMessage(message: error)
        }
    }
}
extension BarCodeViewController: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error.localizedDescription)
        APPSnackbar.getObject().showMessage(message: error.localizedDescription)
        controller.dismiss(animated: true, completion: nil)
    }
}
extension BarCodeViewController: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
