//
//  MainTabBarViewController.swift
//  APP
//
//  Created by Ibrahim on 10/27/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    let languageHandler: AppLanguageHandler = AppLanguageHandler.getObject()
        let utils: AppUtils = AppUtils.getObject()
        let constant: AppConstant = AppConstant.getObject()
        
        var homeVC = UINavigationController()
        var mapVC = UINavigationController()
        var barcodeVC = UINavigationController()
        var newsVC = UINavigationController()
        var contactVC = UINavigationController()
        var selectedTitle = ""
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
           
            initViews()
            
        }
        func initViews() {
          
            self.tabBar.backgroundColor = UIColor.white
            print( self.tabBar.frame.height )
            homeVC = UINavigationController.init(rootViewController: HomeViewController())
            homeVC.navigationBar.isHidden = true
            mapVC = UINavigationController.init(rootViewController: MapViewController())
            mapVC.navigationBar.isHidden = true
            barcodeVC = UINavigationController.init(rootViewController: BarCodeViewController())
            barcodeVC.navigationBar.isHidden = true
            newsVC = UINavigationController.init(rootViewController: NewsViewController())
            newsVC.navigationBar.isHidden = true
            contactVC = UINavigationController.init(rootViewController: ContactUsViewController())
            contactVC.navigationBar.isHidden = true
            let home = HomeViewController()
            let barcode = MyQRController()
            let mapbranch = BranchMapViewController()
            let categ = CategoryViewController()
            let notification = NotificationsViewController()
            self.viewControllers =  [categ ,notification ,barcode, mapbranch , home]
//            for controller in viewControllers! {
//                controller.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
//                controller.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 5.0)
//            }
//            self.tabBar.items?[0].title = ""
//            self.tabBar.items?[0].image = UIImage(named:"delete")?.withRenderingMode(.alwaysOriginal)
//            self.tabBar.items?[1].title = ""
//            self.tabBar.items?[1].image = UIImage(named:"placeholder")?.withRenderingMode(.alwaysOriginal)
//            self.tabBar.items?[2].title = ""
//            self.tabBar.items?[2].image = UIImage(named:"qr-code")?.withRenderingMode(.alwaysOriginal)
//            self.tabBar.items?[3].title = ""
//            self.tabBar.items?[3].image = UIImage(named:"destination")?.withRenderingMode(.alwaysOriginal)
//            self.tabBar.items?[4].title = ""
//            self.tabBar.items?[4].image = UIImage(named:"grid-1")?.withRenderingMode(.alwaysOriginal)
            home.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "homeselect"))
            barcode.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "qr-code")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "qr-codeselect"))
             mapbranch.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "branchmab")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "branchmabselect"))
            categ.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "grid")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "gridselect"))
            notification.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "notif")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "notifpressed"))
            let selectedColor   = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().BLUE_COLOR)
            let unselectedColor = AppUtils.getObject().hexStringToUIColor(hex: "#00AAF8")
            UITabBar.appearance().tintColor = selectedColor
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
            self.selectedIndex = 4
            
            
        }
}
