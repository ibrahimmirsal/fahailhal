//
//  HomePresenter.swift
//  APP
//
//  Created by mac on 8/4/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
import ObjectMapper

class HomePresenter {
    func getNews(successBlock:@escaping (_ model: CategoryResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getNewsUrl(), successBlock: { (response) in
            guard let model = Mapper<CategoryResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getSlider(successBlock:@escaping (_ model: SliderResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        let url = API_URLs.getObject().getSliderURL()
        print(url)
        ServiceHandler.sharedInstance.executeGetServiceWith(url: url, successBlock: { (response) in
            guard let model = Mapper<SliderResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getRegisterDevice(successBlock:@escaping (_ model: SliderResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getRegisterDeviceURL(), successBlock: { (response) in
            guard let model = Mapper<SliderResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getOffers(successBlock:@escaping (_ model: CategoryResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
              
           ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getOffersURL(), successBlock: { (response) in
               guard let model = Mapper<CategoryResponseModel>().map(JSON: response) else {
                   return
               }
               successBlock(model)
           }) { (error) in
               failureBlock(error)
           }
    }
    func getActivities(successBlock:@escaping (_ model: CategoryResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
              
           ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getActivitiesURL(), successBlock: { (response) in
               guard let model = Mapper<CategoryResponseModel>().map(JSON: response) else {
                   return
               }
               successBlock(model)
           }) { (error) in
               failureBlock(error)
           }
    }
    func getBranches(successBlock:@escaping (_ model: BrachesResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getBranchesURL(), successBlock: { (response) in
            guard let model = Mapper<BrachesResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getInvestBranches(successBlock:@escaping (_ model: InvestBranchesResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getInvestBrachesURL(), successBlock: { (response) in
            guard let model = Mapper<InvestBranchesResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getIvestorDetails(nationlid:String ,boxid:String,successBlock:@escaping (_ model: InvestProfiteResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        if nationlid.count == 0 {
            failureBlock(LoginLocalizedString.nationalidRequired)
        }else if boxid.count == 0 {
            failureBlock(LoginLocalizedString.boxidrequired)
        }else {
            ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getInvestDetail(nationalid: nationlid, boxid: boxid), successBlock: { (response) in
                guard let model = Mapper<InvestProfiteResponseModel>().map(JSON: response) else {
                    return
                }
                successBlock(model)
            }) { (error) in
                failureBlock(error)
            }
        }
    }
    func getDiwanis(successBlock:@escaping (_ model: DiwanisResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getDiwanisURL(), successBlock: { (response) in
            guard let model = Mapper<DiwanisResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getDeathOccations(successBlock:@escaping (_ model: OccationResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getDeathOccasionURL(), successBlock: { (response) in
            guard let model = Mapper<OccationResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getMarriageOccations(successBlock:@escaping (_ model: OccationResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getMarriageOccasionURL(), successBlock: { (response) in
            guard let model = Mapper<OccationResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getProducts(code:String,successBlock:@escaping (_ model: ProductsResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        ServiceHandler.sharedInstance.executeGetServiceWith(url: API_URLs.getObject().getBarCodeProductsURL(code: code), successBlock: { (response) in
            guard let model = Mapper<ProductsResponseModel>().map(JSON: response) else {
                return
            }
            successBlock(model)
        }) { (error) in
            failureBlock(error)
        }
    }
    func getContactUS(name:String ,phone:String , email:String , topic : String , message:String,successBlock:@escaping (_ model: BrachesResponseModel) -> Void,failureBlock:@escaping (_ errorMessage: String) -> Void) {
        if name.count == 0 {
            failureBlock(LoginLocalizedString.name_required)
        }else if phone.count == 0 {
            failureBlock(LoginLocalizedString.phone_required)
        }else if phone.count < 8 {
            failureBlock(LoginLocalizedString.invalid_phone)
        }else if email.count == 0 {
            failureBlock(LoginLocalizedString.email_required)
        }else if !AppUtils.getObject().isValidEmail(testStr: email){
            failureBlock(LoginLocalizedString.emailValidateMessage)
        }else if topic.count == 0 {
            failureBlock(LoginLocalizedString.topicrequird)
        }else if message.count == 0 {
            failureBlock(LoginLocalizedString.messagerequired)
        }else {
            let param = NSMutableDictionary()
            param.setValue("\(name)", forKey: "name")
            param.setValue("\(email)", forKey: "email")
            param.setValue("\(phone)", forKey: "phone")
            param.setValue("\(topic)", forKey: "topic")
            param.setValue("\(message)", forKey: "message")
            ServiceHandler.sharedInstance.executePostServiceWith(url:API_URLs.getObject().getContactUSURL(), params:param as! [String : Any], successBlock: { (response) in
                guard let model = Mapper<BrachesResponseModel>().map(JSON: response) else {
                    return
                }
                successBlock(model)
            }) { (error) in
                failureBlock(error)
            }
        }

    }

}
