//
//  PriceListViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class PriceListViewController: UIViewController , AddPriceListProtocol , UITextFieldDelegate{
    
    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var textFieldItemName: LeftTextField!
    @IBOutlet weak var tableViewPriceList: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    var priceList =  NSMutableArray()
    let utils = AppUtils.getObject()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func add(_ sender: Any) {
        let vc = AddListViewController()
        vc.delegade = self
      //  AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.height * 0.24, dismissGesture: true)
        if textFieldItemName.text! != "" {
            addPriceList(title: textFieldItemName.text!)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewAdd.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: "#264B89")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewAdd.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textFieldItemName.text != "" {
            addPriceList(title: textFieldItemName.text!)
        }
        return true
    }
    func addPriceList(title: String) {
        let model = PriceListModel()
        model.title = title
        priceList.add(model)
        utils.setPriceList(vegearr: priceList)
        textFieldItemName.text = ""
        tableViewPriceList.reloadData()
    }
    func initViews () {
        textFieldItemName.placeholder = "آضف قائمة جديدة"
        textFieldItemName.delegate = self
        labelTitle.text = TabBarLocalizedString.shoppinglist
        viewAdd.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR)
        initTable()
        getSavedList()
    }
    func initTable (){
        let nib = UINib(nibName: "PriceListTableViewCell", bundle: nil)
        tableViewPriceList.register(nib, forCellReuseIdentifier: "PriceListTableViewCell")
    }
    func getSavedList (){
       priceList =  utils.getPriceList()
       tableViewPriceList.reloadData()
    }
}
extension PriceListViewController : UITableViewDelegate , UITableViewDataSource , RemovePriceListProtocol{
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
       let hight =  screen.height * 0.07
       return hight
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return priceList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "PriceListTableViewCell"
        let cell: PriceListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PriceListTableViewCell
        cell.index = indexPath.row
        cell.delegade = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.labelName.text = (priceList[indexPath.row] as! PriceListModel).title
        return cell
    }
    func removePriceList(index:Int) {
        priceList.removeObject(at: index)
        utils.setPriceList(vegearr: priceList)
        tableViewPriceList.reloadData()
    }
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ProductsListViewController()
        vc.listTitle = (priceList[indexPath.row] as! PriceListModel).title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
