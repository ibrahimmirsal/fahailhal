//
//  PriceListTableViewCell.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol RemovePriceListProtocol {
    func removePriceList(index:Int)
}
class PriceListTableViewCell: UITableViewCell {
    var index = 0
    var delegade : RemovePriceListProtocol?
    @IBOutlet weak var labelName: LeftLBL!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func remove(_ sender: Any) {
        self.delegade?.removePriceList(index: index)
    }
    
}
