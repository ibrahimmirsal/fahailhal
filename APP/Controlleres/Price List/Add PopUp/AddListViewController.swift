//
//  AddListViewController.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol AddPriceListProtocol {
    func addPriceList(title:String)
}
class AddListViewController: UIViewController {
    var delegade : AddPriceListProtocol?
    @IBOutlet weak var textFieldTitle: LeftTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func add(_ sender: Any) {
        if textFieldTitle.text!.count > 0 {
            self.delegade?.addPriceList(title: textFieldTitle.text!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
