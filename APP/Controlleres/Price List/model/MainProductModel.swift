//
//  MainProductModel.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
class MainProductModel : NSObject , NSCoding {
    var title: String = ""
    var selected = ""
    override init() {
        title = ""
        selected = ""
    }
    required init(coder decoder: NSCoder) {
        self.title = decoder.decodeObject(forKey: "mainProductTitle") as? String ?? ""
        self.selected = decoder.decodeObject(forKey: "mainProducttselect") as? String ?? ""
        
    }
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "mainProductTitle")
        coder.encode(selected, forKey: "mainProducttselect")
    }
    
}
