//
//  PriceListModel.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
class PriceListModel : NSObject , NSCoding {
    var title: String = ""
    override init() {
        title = ""
    }
    required init(coder decoder: NSCoder) {
        self.title = decoder.decodeObject(forKey: "pricelistTitle") as? String ?? ""
    }
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "pricelistTitle")
    }
    
}
