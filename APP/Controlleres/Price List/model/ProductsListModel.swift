//
//  ProductsListModel.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import Foundation
class ProductsListModel : NSObject , NSCoding {
    var title: String = ""
    var selected = ""
    override init() {
        title = ""
        selected = ""
    }
    required init(coder decoder: NSCoder) {
        self.title = decoder.decodeObject(forKey: "productlistTitle") as? String ?? ""
        self.selected = decoder.decodeObject(forKey: "productlistselect") as? String ?? ""

    }
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "productlistTitle")
        coder.encode(selected, forKey: "productlistselect")
    }
    
}
