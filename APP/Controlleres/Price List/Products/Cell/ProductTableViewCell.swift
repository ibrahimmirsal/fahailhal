//
//  ProductTableViewCell.swift
//  APP
//
//  Created by mac on 11/26/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol AddRemoveProductProtocol {
    func getAddRemoveProduct(index:Int)
    
}
class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var viewSelect: ShadowView!
    @IBOutlet weak var btnProductSelect: UIButton!
    @IBOutlet weak var labelProductName: LeftLBL!
    var index = 0
    var delegade : AddRemoveProductProtocol?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func addremove(_ sender: Any) {
        self.delegade?.getAddRemoveProduct(index: index)
    }
    
}
