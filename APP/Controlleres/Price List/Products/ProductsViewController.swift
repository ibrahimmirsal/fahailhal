//
//  ProductsViewController.swift
//  APP
//
//  Created by mac on 11/26/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableViewProducts: UITableView!
    var productsArray = NSMutableArray()
    let utils = AppUtils.getObject()
    var productsList = NSMutableArray()
    var listTitle = ""
    var productlistindex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initViews()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    func initViews () {
        labelTitle.text = "الآصناف"
        initTable()
        getSavedList()
    }
    func initTable (){
        let nib = UINib(nibName: "ProductTableViewCell", bundle: nil)
        tableViewProducts.register(nib, forCellReuseIdentifier: "ProductTableViewCell")
    }
    func getSavedList (){
        productsArray =  utils.getMainProductList()
        if productsArray.count == 0 {
            let product = ProductsListModel()
            product.title = "شوكولا"
            product.selected = "0"
            productsArray.add(product)
            let product2 = ProductsListModel()
            product2.title = "شيبسي"
            product2.selected = "0"
            productsArray.add(product2)
            let product3 = ProductsListModel()
            product3.title = "حلويات"
            product3.selected = "0"
            productsArray.add(product3)
            let product4 = ProductsListModel()
            product4.title = "منتجات الرجيم"
            product4.selected = "0"
            productsArray.add(product4)
            let product5 = ProductsListModel()
            product5.title = "بسكويت"
            product5.selected = "0"
            productsArray.add(product5)
            let product6 = ProductsListModel()
            product6.title = "كورن فليكس"
            product6.selected = "0"
            productsArray.add(product6)
            let product7 = ProductsListModel()
            product7.title = "كرواسان"
            product7.selected = "0"
            productsArray.add(product7)
            let product8 = ProductsListModel()
            product8.title = "خبز"
            product8.selected = "0"
            productsArray.add(product8)
            let product9 = ProductsListModel()
            product9.title = "توست"
            product9.selected = "0"
            productsArray.add(product9)
            let product10 = ProductsListModel()
            product10.title = "صمون"
            product10.selected = "0"
            productsArray.add(product10)
            let product11 = ProductsListModel()
            product11.title = "شابورة"
            product11.selected = "0"
            productsArray.add(product11)
            let product12 = ProductsListModel()
            product12.title = "شاي"
            product12.selected = "0"
            productsArray.add(product12)
            utils.setMainProductList(vegearr: productsArray)
        }
        for product in productsArray as! [ProductsListModel] {
            for prodlist in productsList as! [ProductsListModel] {
                if product.title == prodlist.title
                {
                    product.selected = "1"
                    break
                }
            }
        }
        tableViewProducts.reloadData()
    }
}

extension ProductsViewController : UITableViewDelegate , UITableViewDataSource , AddRemoveProductProtocol{
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        let hight =  screen.height * 0.07
        return hight
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return productsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "ProductTableViewCell"
        let cell: ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ProductTableViewCell
        cell.index = indexPath.row
        cell.delegade = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let model = productsArray.object(at: indexPath.row) as! ProductsListModel
        cell.labelProductName.text = model.title
        if model.selected == "1" {
            cell.btnProductSelect.setImage(UIImage.init(named: "selectlist"), for: .normal)
        }else {
            cell.btnProductSelect.setImage(UIImage.init(named: "unselectlist"), for: .normal)
        }
        return cell
    }
    func getAddRemoveProduct(index:Int) {
        let model = productsArray.object(at: index) as! ProductsListModel
        if model.selected  == "1"{
            model.selected = "0"
        }else {
            model.selected = "1"
        }
        productsArray.replaceObject(at: index, with: model)
        if checkProductList(title: model.title){
            productsList.removeObject(at: productlistindex)
        }else {
            productsList.add(model)
        }
        utils.setProductList(vegearr: productsList, key: listTitle)
        tableViewProducts.reloadData()
    }
    func checkProductList (title:String) -> Bool {
        var fount = false
        var index = 0
        for prodlist in productsList as! [ProductsListModel] {
            if title == prodlist.title
            {
                fount = true
                break
            }
         index = index + 1
        }
        productlistindex = index
        return fount
    }

    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = productsArray.object(at: indexPath.row) as! ProductsListModel
        if model.selected  == "1"{
            model.selected = "0"
        }else {
            model.selected = "1"
        }
        productsArray.replaceObject(at: indexPath.row, with: model)
        if checkProductList(title: model.title){
            productsList.removeObject(at: productlistindex)
        }else {
            productsList.add(model)
        }
        utils.setProductList(vegearr: productsList, key: listTitle)
        tableViewProducts.reloadData()
    }
    
}

