//
//  AddProdoctPopUpViewController.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol AddProductListProtocol {
    func addProductList(title:String)
    func moreProducts ()
}

class AddProdoctPopUpViewController: UIViewController {
    var delegade : AddProductListProtocol?

    @IBOutlet weak var textFieldTitle: LeftTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func clse(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func add(_ sender: Any) {
        if textFieldTitle.text!.count > 0 {
            self.delegade?.addProductList(title: textFieldTitle.text!)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func more(_ sender: Any) {
        self.delegade?.moreProducts()
        self.dismiss(animated: true, completion: nil)

    }
}
