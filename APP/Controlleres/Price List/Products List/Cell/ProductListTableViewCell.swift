//
//  ProductListTableViewCell.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
protocol RemoveProductListProtocol {
    func removeProductList(index:Int)
    func selectProductList(index:Int)

}

class ProductListTableViewCell: UITableViewCell {
    @IBOutlet weak var viewSelect: ShadowView!
    var index = 0
    var delegade : RemoveProductListProtocol?
    @IBOutlet weak var labelTitle: LeftLBL!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func prodselect(_ sender: Any) {
        self.delegade?.selectProductList(index: index)
    }
    @IBAction func remove(_ sender: Any) {
        self.delegade?.removeProductList(index: index)

    }
}
