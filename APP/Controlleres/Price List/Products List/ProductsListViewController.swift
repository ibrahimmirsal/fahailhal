//
//  ProductsListViewController.swift
//  APP
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class ProductsListViewController: UIViewController , AddProductListProtocol , UITextFieldDelegate {
    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var textFieldProductName: LeftTextField!
    @IBOutlet weak var tableViewProductPriceList: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    var priceList =  NSMutableArray()
    let utils = AppUtils.getObject()
    var listTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initViews()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func add(_ sender: Any) {
//        let vc = AddProdoctPopUpViewController()
//        vc.delegade = self
//      //  AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.height * 0.24, dismissGesture: true)
//        if textFieldProductName.text! != "" {
//            addProductList(title: textFieldProductName.text!)
//        }else {
//
//        }
        let vc = ProductsViewController()
        vc.listTitle = listTitle
        vc.productsList = priceList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func addProductList(title:String) {
        let model = ProductsListModel()
        model.title = title
        model.selected = "0"
        priceList.add(model)
         utils.setProductList(vegearr: priceList, key: listTitle)
        textFieldProductName.text = ""
        tableViewProductPriceList.reloadData()
    }
    func moreProducts() {
        
    }
    func initViews () {
        labelTitle.text = listTitle
        textFieldProductName.placeholder = "آضف صنف جديد"
        textFieldProductName.delegate = self
        initTable()
        getSavedList()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewAdd.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: "#264B89")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewAdd.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textFieldProductName.text != "" {
            addProductList(title: textFieldProductName.text!)
        }
        return true
    }
    func initTable (){
        let nib = UINib(nibName: "ProductListTableViewCell", bundle: nil)
        tableViewProductPriceList.register(nib, forCellReuseIdentifier: "ProductListTableViewCell")
    }
    func getSavedList (){
        priceList =  utils.getProductList(key: listTitle)
        tableViewProductPriceList.reloadData()
    }
}
extension ProductsListViewController : UITableViewDelegate , UITableViewDataSource , RemoveProductListProtocol{
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        let hight =  screen.height * 0.07
        return hight
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return priceList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "ProductListTableViewCell"
        let cell: ProductListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ProductListTableViewCell
        cell.index = indexPath.row
        cell.delegade = self
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let model = priceList.object(at: indexPath.row) as! ProductsListModel
        cell.labelTitle.text = model.title
        if model.selected == "1" {
            cell.viewSelect.backgroundColor = AppUtils.getObject().hexStringToUIColor(hex: AppConstant.getObject().LIGHT_GRAY_COLOR)
        }else {
           cell.viewSelect.backgroundColor = .white
        }
        return cell
    }
     func removeProductList(index:Int) {
        priceList.removeObject(at: index)
        utils.setProductList(vegearr: priceList, key: listTitle)
        tableViewProductPriceList.reloadData()
    }
    func selectProductList(index: Int) {
        let model = priceList.object(at: index) as! ProductsListModel
        if model.selected  == "1"{
            model.selected = "0"
        }else {
            model.selected = "1"
        }
        priceList.replaceObject(at: index, with: model)
        utils.setProductList(vegearr: priceList, key: listTitle)
        tableViewProductPriceList.reloadData()
        
    }
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = priceList.object(at: indexPath.row) as! ProductsListModel
        if model.selected  == "1"{
            model.selected = "0"
        }else {
            model.selected = "1"
        }
        priceList.replaceObject(at: indexPath.row, with: model)
        tableViewProductPriceList.reloadData()
    }
    
}
