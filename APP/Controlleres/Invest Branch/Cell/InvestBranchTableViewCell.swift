//
//  InvestBranchTableViewCell.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class InvestBranchTableViewCell: UITableViewCell {

    @IBOutlet weak var labelInvestBranchName: LeftLBL!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func drawCell (model : InvestBranchModel) {
        labelInvestBranchName.text = model.store_name
    }
    func drawDiwanCell (model:InnerDayModel) {
        labelInvestBranchName.text = model.name
    }
    func drawOccationCell(model:OccationModel) {
        labelInvestBranchName.text = model.title
    }
    func drawCellBranch(model : BranchModel) {
        labelInvestBranchName.text = model.branch_name
    }
}
