//
//  InvestBranchDetailsPopUpViewController.swift
//  APP
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class InvestBranchDetailsPopUpViewController: UIViewController {
    @IBOutlet weak var labelName: LeftLBL!
    @IBOutlet weak var labelPhone: LeftLBL!
    @IBOutlet weak var labelWorkingHours: LeftLBL!
    @IBOutlet weak var labelNameValue: LeftLBL!
    @IBOutlet weak var labelPhoneValue: LeftLBL!
    @IBOutlet weak var labelWorkingHourValue: LeftLBL!
    var model = InvestBranchModel()
    var product  = ProductModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initViews () {
        if product.product_id > 0 {
            labelName.text = LoginLocalizedString.name
            labelNameValue.text = product.product_name
            labelPhone.text = LoginLocalizedString.price
            labelPhoneValue.text = "\(product.product_list_price)"
            labelWorkingHours.text = LoginLocalizedString.discount
            labelWorkingHourValue.text = "\(product.product_discount)"
        }else {
            labelName.text = LoginLocalizedString.name
            labelNameValue.text = model.store_name
            labelPhone.text = LoginLocalizedString.phone
            labelPhoneValue.text = model.store_phone
            labelWorkingHours.text = LoginLocalizedString.workinghours
            labelWorkingHourValue.text = model.store_open_time
        }

    }
}
