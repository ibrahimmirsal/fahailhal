//
//  InvestBranchesViewController.swift
//  APP
//
//  Created by mac on 11/11/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit

class InvestBranchesViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableViewInvestBranch: UITableView!
    var branchArray = [InvestBranchModel]()
    let indicator = APPLoader.getObject()
    let presenter = HomePresenter()
    var vcTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initViews () {
        labelTitle.text = vcTitle
        initTable()
        getData()
    }
    func initTable (){
        let nib = UINib(nibName: "InvestBranchTableViewCell", bundle: nil)
        tableViewInvestBranch.register(nib, forCellReuseIdentifier: "InvestBranchTableViewCell")
    }
    func getData () {
        indicator.show(self.view, true)
        presenter.getInvestBranches(successBlock: { (model) in
            self.indicator.hide()
            self.branchArray = model.branches
            self.tableViewInvestBranch.reloadData()
        }) { (error) in
            self.indicator.hide()
        }
    }
}
extension InvestBranchesViewController : UITableViewDelegate , UITableViewDataSource {
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screen = UIScreen.main.bounds
        return screen.height * 0.05
        
    }
    open func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return branchArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "InvestBranchTableViewCell"
        let cell: InvestBranchTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! InvestBranchTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.drawCell(model: branchArray[indexPath.row])
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       let vc = InvestBranchDetailsPopUpViewController()
        vc.model = branchArray[indexPath.row]
        AppPopUpHandler.getObject().getPopUpView(VC: vc, hight: UIScreen.main.bounds.size.height * 0.3, dismissGesture: true)
    }
    
}

