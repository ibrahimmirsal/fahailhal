//
//  AboutUsViewController.swift
//  APP
//
//  Created by mac on 11/10/19.
//  Copyright © 2019 ibrahim. All rights reserved.
//

import UIKit
import WebKit
class AboutUsViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var labelTitle: UILabel!
    var vcTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func initView () {
        labelTitle.text = vcTitle
        let myURLString = vcTitle == TabBarLocalizedString.aboutus ? API_URLs.getObject().getAboutURL() : API_URLs.getObject().getCallUSURL()
        guard let myURL = URL(string: myURLString) else {
            print("Error: \(myURLString) doesn't seem to be a valid URL")
            return
        }
        do {
            let myHTMLString = try String(contentsOf: myURL, encoding: .utf8)
            print("HTML : \(myHTMLString)")
            webView.loadHTMLString(myHTMLString, baseURL: nil)
        } catch let error {
            print("Error: \(error)")
        }
        
    }
    
 

}
