//
//  WebViewController.swift
//  APP
//
//  Created by Ibrahim Sabry on 22/02/2023.
//  Copyright © 2023 ibrahim. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWeb()
        // Do any additional setup after loading the view.
    }

    @IBAction func backOnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadWeb() {
        let link = "http://fahaheel.semaphore.me"
        if let url  = URL(string: link) {
            webView.load(URLRequest(url: url))
        }
    }
    
    @IBAction func refresh(_ sender: Any) {
        loadWeb()
    }
    
}
