//
//  MissingPopUpViewController.swift
//  automative
//
//  Created by mac on 3/29/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class MissingPopUpViewController: UIViewController {
    @IBOutlet weak var btnConfirm: RoundedBtn!
    @IBOutlet weak var labelMessage: CenterLBL!
    var message = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        labelMessage.text = message
        btnConfirm .setTitle(AlertLocalizedString.ok, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func done(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
